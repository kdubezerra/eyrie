/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - carlos.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.eyrie;

import java.util.ArrayList;
import java.util.List;

import ch.usi.dslab.bezerra.eyrie.exec.OptimisticStateMachine;
import ch.usi.dslab.bezerra.netwrapper.Message;

public class Command extends Message {
   private static final long serialVersionUID = 1L;

   public static boolean executionIsOptimistic() {
      StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
      for (StackTraceElement el : stackTrace) {
         String className  = el.getClassName();
         if (className.contains(OptimisticStateMachine.class.getName()))
            return true;
      }
      return false;
   } 
   
   // in the form client.seq
   public CmdId id;
   
   List<Integer> destinations;
   transient List<Partition> destinationPartitions = null;
//   Object prophecyContext;

   public Command() {
      
   }
   
   public Command(Object... objs) {
      super(objs);
   }
   
   public CmdId getId() {
      return id;
   }
   
   public void setId(CmdId id) {
      this.id = id;
   }
   
   public void setId(int clientId, long cliCmdSeq) {
      this.id = new CmdId(clientId, cliCmdSeq);
   }
   
   public int getSourceClientId() {
      return id.clientId;
   }
   
   synchronized public List<Partition> getDestinations() {
      if (destinationPartitions == null) {
         destinationPartitions = new ArrayList<Partition>(destinations.size());
         for (int partid : destinations)
            destinationPartitions.add(Partition.getPartition(partid));
      }
      return destinationPartitions;
   }
   
   synchronized public void setDestinations(List<Partition> dests) {
      destinationPartitions = dests;
      destinations = new ArrayList<Integer>(dests.size());
      for (Partition p : dests) {
         destinations.add(p.getId());
      }
   }
   
   @Override
   public boolean equals(Object other) {
      return this.id.equals( ((Command)other).id );
   }
   
}
