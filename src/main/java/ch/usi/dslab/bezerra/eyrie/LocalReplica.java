/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.eyrie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import ch.usi.dslab.bezerra.eyrie.exec.CommandExecution;
import ch.usi.dslab.bezerra.eyrie.exec.OptimisticStateMachine;
import ch.usi.dslab.bezerra.mcad.FastMulticastAgent;
import ch.usi.dslab.bezerra.mcad.Group;
import ch.usi.dslab.bezerra.mcad.MulticastServer;
import ch.usi.dslab.bezerra.netwrapper.Message;

public class LocalReplica {
   
   public static class PeekWaitBlockingQueue<E> extends LinkedBlockingDeque<E> {
      private static final long serialVersionUID = 1L;
      
      synchronized public E peekWait() throws InterruptedException {
         E obj = take();
         this.addFirst(obj);
         return obj;
      }
      
      public void put(E obj) throws InterruptedException {
         super.put(obj);
      }

      synchronized public E remove() {
         return super.removeFirst();
      }
   }
   
   public static class ConservativeDeliverer implements Runnable {
      
      LocalReplica localReplica;
      
      public ConservativeDeliverer(LocalReplica localReplica) {
         this.localReplica = localReplica;
      }

      // ===========================================
      // ===========================================

      // ==============================================================
      // LocalReplica's thread for getting and sorting mcast messages
      // ==============================================================
      @Override
      public void run() {
         
         localReplica.waitForStateMachineToBeReady();
         
         while (localReplica.running) {
            Message m = localReplica.multicastServer.getMulticastAgent().deliverMessage();
//            Message m = localReplica.multicastServer.getMulticastAgent().deliverMessage().deepDuplicate();
            
            MessageType type = (MessageType) m.getNext();
//            System.out.println(String.format("cons-delivered message %s", m));
            switch (type) {
               
               case COMMAND: {
                  Command c = (Command) m.getNext(); // cmd (inside cmdContainer!)
                  c.t_learner_delivered = System.currentTimeMillis();
                  
//                  System.out.println(String.format("cons-delivered command %s", c.getId()));
                  
//                  c.t_client_send = m.t_client_send;
//                  c.t_batch_ready = m.t_batch_ready;               
//                  c.piggyback_proposer_serialstart = m.piggyback_proposer_serialstart;
//                  c.piggyback_proposer_serialend   = m.piggyback_proposer_serialend;               
//                  c.t_learner_delivered = m.t_learner_delivered;
//                  c.t_learner_deserialized = m.t_learner_deserialized;
//                  c.t_command_enqueued = System.currentTimeMillis();
                  try {
                     // Multicast signals here already
                     StateMachine consSM = StateMachine.getMachine();
                     consSM.sendSignals(c);
                     
                     // ==============================
                     // with super early state sending
                     CommandExecution ce = new CommandExecution(c, localReplica);
                     ce.trySendingObjectsBeforeExecution(localReplica.pendingConservativeExecutions, consSM);
                     synchronized (localReplica.pendingConservativeExecutions) {
                        localReplica.pendingConservativeExecutions.add(ce);                        
                     }
                     localReplica.conservativeExecutionQueue.put(ce);
                     // ==============================
                     // previous eyrie implementation
//                     localReplica.conservativeCommandsQueue.put(c);
                     // ==============================
                     
                  } catch (InterruptedException e) {
                     e.printStackTrace();
                  }
                  break;
               }

               case RETURN_VALUE: {
                  Object retval = m.getNext();
                  try {
                     localReplica.returnValuesQueue.put(retval);
                  } catch (InterruptedException e) {
                     e.printStackTrace();
                  }
                  break;
               }

               default: {
                  break;
               }
            }
         }
         
      }
      
   }
   
   public static class FastDeliverer implements Runnable {

      LocalReplica localReplica;
      
      public FastDeliverer(LocalReplica localReplica) {
         this.localReplica = localReplica;
      }

      // ===========================================
      // ===========================================

      // ==============================================================
      // LocalReplica's thread for getting and sorting mcast messages
      // ==============================================================
      @Override
      public void run() {
         FastMulticastAgent oma = (FastMulticastAgent) localReplica.multicastServer.getMulticastAgent();

         // ===================
         // simulating mistakes
//         Random screwer = new Random(System.currentTimeMillis());
//         List<Command> screwed = new ArrayList<Command>();
//         final int pScrew = 0; // p(screw) = probability of screwing up.
         // ===================

         while (localReplica.running) {
            Message m = oma.deliverMessageFast();
//            Message m = oma.deliverMessageFast().deepDuplicate();
//            m.rewind();
            
            MessageType type = (MessageType) m.getNext();
//            System.out.println(String.format("fast-delivered message %s", m));
            
            switch (type) {
               case COMMAND: {
                  Command c = (Command) m.getNext(); // cmd (inside cmdContainer!)
                  c.t_learner_delivered = System.currentTimeMillis();

//                  System.out.println(String.format("fast-delivered %s", c.getId()));

//                  c.t_client_send = m.t_client_send;
//                  c.t_batch_ready = m.t_batch_ready;               
//                  c.piggyback_proposer_serialstart = m.piggyback_proposer_serialstart;
//                  c.piggyback_proposer_serialend   = m.piggyback_proposer_serialend;               
//                  c.t_learner_delivered = m.t_learner_delivered;
//                  c.t_learner_deserialized = m.t_learner_deserialized;
//                  c.t_command_enqueued = System.currentTimeMillis();
                  try {
                     // Multicast signals here already
                     // no signals for optimistic execution
                     // StateMachine.sendSignals(c);
//                     if (screwer.nextInt(100) < pScrew) {
//                        screwed.add(c);
//                        System.out.println("LocalReplica :: ================================================= :: screwed opt delivery; screwed elements: " + screwed.size());
//                     }
//                     else
//                     {
//                        Collections.shuffle(screwed);
//                        localReplica.fastCommandsQueue.addAll(screwed);
//                        screwed.clear();
                        localReplica.fastCommandsQueue.put(c);
//                     }

                  } catch (InterruptedException e) {
                     e.printStackTrace();
                  }
                  break;
               }
               
               case OBJ_CONS: {
                  m.rewind();
                  localReplica.handleConservativeObjectUpdateMessage(m);
                  break;
               }

               case OBJ_CONS_BUNDLE : {
                  while (m.hasNext()) {
                     Message objMsg = (Message) m.getNext();
                     objMsg.rewind();
                     localReplica.handleConservativeObjectUpdateMessage(objMsg);
                  }
                  break;
               }
               
               case OBJ_OPT: {
                  m.rewind();
                  localReplica.handleOptimisticObjectUpdateMessage(m);
                  break;
               }
               
               case OBJ_OPT_BUNDLE : {
                  while (m.hasNext()) {
                     Message objMsg = (Message) m.getNext();
                     objMsg.rewind();
                     localReplica.handleOptimisticObjectUpdateMessage(objMsg);
                  }
                  break;
               }
               
               case SIGNAL: {
                  StateMachine consSM = StateMachine.getMachine();
                  consSM.enqueueSignal(m);
                  break;
               }
               
               case OPTEXEC_STATUS : {
                  CmdId cmdId       = (CmdId)   m.getNext();
                  int   partitionId = (Integer) m.getNext();
                  int   status      = (Integer) m.getNext();
                  OptimisticStateMachine osm = OptimisticStateMachine.getMachine();
                  osm.setPartitionExecutionStatus(cmdId, partitionId, status);
                  localReplica.receivedOptObjectDiffs.notifyRemoteStatus();
                  break;
               }

               default: {
                  break;
               }
            }
         }
      }
      
   }

   public static class ObjectDiffRepository {
      public static final Message ERROR_DIFF = new Message();
      
      Map<CmdId, Map<ObjId, Message>> allObjsReceived;
      Map<CmdId, Integer> pendingCollectors;
      int numCollectors;
      
      public ObjectDiffRepository() {
         this(1);
      }
      
      public ObjectDiffRepository(int numCollectors) {
         setNumCollectors(numCollectors);
         allObjsReceived = new ConcurrentHashMap<CmdId, Map<ObjId,Message>>();
         pendingCollectors = new ConcurrentHashMap<CmdId, Integer>();
      }
      
      public void setNumCollectors(int numCollectors) {
         this.numCollectors = numCollectors;
      }
      
      synchronized public void storeObjectDiff(CmdId cmdId, ObjId objId, Message objDiff) {
//         synchronized (allObjsReceived) {
            Map<ObjId, Message> cmdObjDiffs = allObjsReceived.get(cmdId);

            if (cmdObjDiffs == null) {
               cmdObjDiffs = new HashMap<ObjId, Message>();
               allObjsReceived.put(cmdId, cmdObjDiffs);
            }

            cmdObjDiffs.put(objId, objDiff);
            if (pendingCollectors.containsKey(cmdId) == false)
               pendingCollectors.put(cmdId, numCollectors);
//            allObjsReceived.notifyAll();
            notifyAll();
//         }
//         System.out.println(String.format("%d cmds with diffs received.", allObjsReceived.size()));
      }
      
      synchronized public void notifyRemoteStatus() {
         notifyAll();
      }
      
      synchronized public Message takeObjectDiff(CmdId cmdId, ObjId objId, boolean optimistic) {
         // outter map: id is cmd's
         // inner  map: id is object's
         Message wantedObjectDiff = null;
//         synchronized (allObjsReceived) {
            boolean gotIt = false;
            boolean errorReceived = false;
            try {
               do {
                  if (optimistic) {
                     OptimisticStateMachine osm = OptimisticStateMachine.getMachine();
                     errorReceived = osm.checkForRemoteError(cmdId);
                     if (errorReceived) return ObjectDiffRepository.ERROR_DIFF;
                  }
                  
                  Map<ObjId, Message> currentCommandReceivedObjectDiffs = allObjsReceived.get(cmdId);
                  if (currentCommandReceivedObjectDiffs == null) {
//                     allObjsReceived.wait();
                     wait();
                     continue;
                  }
                  
                  wantedObjectDiff = currentCommandReceivedObjectDiffs.get(objId);
                  if (wantedObjectDiff == null) {
//                     allObjsReceived.wait();
                     wait();
                     continue;
                  }
                  
                  gotIt = true;
               } while (gotIt == false);
            }
            catch (InterruptedException e) {
               System.err.println("!!! - something occurred while trying to get object from Map<Map<Object>>");
               e.printStackTrace();
               System.exit(1);
            }
         
         
//         }
         
         wantedObjectDiff.rewind();
         return wantedObjectDiff;
      }
      
      synchronized public void markAsDone(CmdId cmdId) {
         if (pendingCollectors.containsKey(cmdId) == false) {
            createDummyEntry(cmdId);
         }
         
         int pending = pendingCollectors.get(cmdId);
         
         if (pending > 1)
            pendingCollectors.put(cmdId, pending - 1);
         else {
            pendingCollectors.remove(cmdId);
            allObjsReceived.remove(cmdId);
         }
      }
      
      private Map<ObjId, Message> dummyEntryValue = new HashMap<ObjId, Message>();
      
      private void createDummyEntry(CmdId cmdId) {
         allObjsReceived.put(cmdId, dummyEntryValue);
         pendingCollectors.put(cmdId, numCollectors);
      }
   }
   
   public enum MessageType{
      // TODO: make the serialization of this enum to a byte, instead of String (default)
      COMMAND, RETURN_VALUE, OBJ_CONS, OBJ_CONS_BUNDLE, SIGNAL, OBJ_OPT, OBJ_OPT_BUNDLE, REPLY_OPT, REPLY_CONS_FULL, REPLY_CONS_OPT_CONF, OPTEXEC_STATUS
   }

   // TODO: this class should contain the necessary stuff to send and receive
   // messages
   static LocalReplica replica = null;
   final Queue<CommandExecution> pendingConservativeExecutions;
   final BlockingQueue<CommandExecution> conservativeExecutionQueue;
   final BlockingQueue<Command> conservativeCommandsQueue;
   final BlockingQueue<Object>  returnValuesQueue;
   final BlockingQueue<Command> optimisticCommandsQueue;
   final BlockingQueue<Object>  optimisticReturnValuesQueue;
   final BlockingQueue<Command> fastCommandsQueue;
   final BlockingQueue<Object>  fastReturnValuesQueue;
   
   Thread conservativeDelivererThread;
   Thread fastDelivererThread;

   MulticastServer multicastServer;
   boolean running = true;
   Partition localPartition = null;
   
//   boolean partitionMulticaster = false;
   
   ObjectDiffRepository receivedConsObjectDiffs = new ObjectDiffRepository();
   ObjectDiffRepository receivedOptObjectDiffs  = new ObjectDiffRepository();

   public Partition getLocalPartition() {
      return localPartition;
   }

   // ===========================================
   // LocalReplica construction-related methods
   // ===========================================
   private LocalReplica() {
      pendingConservativeExecutions = new LinkedList<CommandExecution>();
      conservativeExecutionQueue = new LinkedBlockingQueue<CommandExecution>();
      
      conservativeCommandsQueue = new LinkedBlockingQueue<Command>();
      optimisticCommandsQueue = new LinkedBlockingQueue<Command>();
      fastCommandsQueue = new LinkedBlockingQueue<Command>();
      
      returnValuesQueue = new LinkedBlockingQueue<Object>();
      optimisticReturnValuesQueue = new LinkedBlockingQueue<Object>();
      fastReturnValuesQueue = new LinkedBlockingQueue<Object>();
   }

   public static LocalReplica getLocalReplica() {
      return replica;
   }
   
   public void waitForStateMachineToBeReady() {
      try {
         Semaphore smReady = StateMachine.getMachine().getReadySemaphore();
         smReady.acquire();
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }
   
   public void setNumConsObjCollectors(int numCollectors) {
      receivedConsObjectDiffs.setNumCollectors(numCollectors);
   }
   
   public void notifyDoneWithConsDiff(CmdId cmdId) {
      receivedConsObjectDiffs.markAsDone(cmdId);
   }
   
   public void notifyDoneWithOptDiff(CmdId cmdId) {
      receivedOptObjectDiffs.markAsDone(cmdId);
   }

   private void handleConservativeObjectUpdateMessage(Message msg) {
      @SuppressWarnings("unused")
      MessageType type = (MessageType) msg.getNext();
      CmdId   cmdId    = (CmdId  )     msg.getNext();
      ObjId   objId    = (ObjId  )     msg.getNext();
      Message objDiff  = (Message)     msg.getNext();
      
//      System.out.println("storing cons object diff for cmdId = " + cmdId);
      
      receivedConsObjectDiffs.storeObjectDiff(cmdId, objId, objDiff);
   }

   private void handleOptimisticObjectUpdateMessage(Message msg) {
      @SuppressWarnings("unused")
      MessageType type = (MessageType) msg.getNext();
      CmdId   cmdId    = (CmdId  )     msg.getNext();
      ObjId   objId    = (ObjId  )     msg.getNext();
      Message objDiff  = (Message)     msg.getNext();

//      System.out.println("storing opt object diff for cmdId = " + cmdId);
      
      receivedOptObjectDiffs.storeObjectDiff(cmdId, objId, objDiff);
   }

   // ==============================================================
   // ==============================================================

   // ========================================================================
   // Lowest level for communication used by the local replica; uses mcagent
   // ========================================================================
   public void multicastCommand(List<Partition> dests, Command cmd) {
      cmd.setDestinations(dests);
      Message msg = new Message(MessageType.COMMAND, cmd);
      LocalReplica.replica.multicast(dests, msg);
   }
   
   public void multicastCommand(Command cmd) {
      StateMachine consSM = StateMachine.getMachine();
      List<Partition> destinations = consSM.oracle.getProphecy(cmd).destinations;
      multicastCommand(destinations, cmd);
   }

   public void multicast(Partition part, Message msg) {
      // send message through the multicast agent
      multicastServer.getMulticastAgent().multicast(part.pgroup, msg);
   }

   public void multicast(List<Partition> dests, Message msg) {
      if (dests.isEmpty())
         return;
      
      // creating the array of destination groups to pass to the multicast agent
      ArrayList<Group> groupdests = new ArrayList<Group>(dests.size());
      for (Partition part : dests)
         groupdests.add(part.pgroup);

      // add info about the message's destinations
      // ArrayList<Integer> destids = new ArrayList<Integer>(dests.size());
      // for (Partition part : dests)
      // destids.add(part.getId());
      // msg.destinationPartitionsIDs = destids;

      // send message through the multicast agent
      multicastServer.getMulticastAgent().multicast(groupdests, msg);
   }

   public void multicastObject(List<Partition> dests, Message msg) {
      multicast(dests, msg);
   }

   public void multicastLocalEarlyObjects(CommandExecution cmdExecution, boolean optimistic) {
      StateMachine SM = StateMachine.getMachine();
      
      Command cmd = cmdExecution.getCommand();
      List<ObjId> earlyObjectsIds = SM.getOracle().getEarlyObjectsIds(cmd);
      if (earlyObjectsIds == null) return;
      earlyObjectsIds.removeAll(cmdExecution.getExchangedObjects());
      if (earlyObjectsIds.isEmpty()) return;

      List<Partition> remoteDestinations = new ArrayList<Partition>(cmd.getDestinations());
      remoteDestinations.remove(localPartition);
      if (remoteDestinations.isEmpty()) return;

      List<PRObject> localEarlyObjects = new ArrayList<PRObject>(earlyObjectsIds.size());
      for (ObjId oid : earlyObjectsIds) localEarlyObjects.add(PRObject.getObjectById(oid, optimistic));
      Iterator<PRObject> iter = localEarlyObjects.iterator();
      while (iter.hasNext()) if (iter.next().isLocal() == false) iter.remove();
      if (localEarlyObjects.isEmpty()) return;
      
      // TODO: optimize this early objects bundle... too many Message objects inside it...
      MessageType msgType = optimistic ? MessageType.OBJ_OPT_BUNDLE : MessageType.OBJ_CONS_BUNDLE;
      MessageType objType = optimistic ? MessageType.OBJ_OPT        : MessageType.OBJ_CONS;
      Message earlyObjectsBundle = new Message(msgType);
      for (PRObject obj : localEarlyObjects) {
         Message objectDiff = obj.getSuperDiff(remoteDestinations);
         Message objectMessage = new Message(objType, cmd.id, obj.id, objectDiff);
         earlyObjectsBundle.addItems(objectMessage);
         cmdExecution.addExchangedObject(obj.id);
      }

      multicast(remoteDestinations, earlyObjectsBundle);
   }

   public Message deliver() {
      Message message = multicastServer.getMulticastAgent().deliverMessage();
      return message;
   }

   // ========================================================================
   // ========================================================================

   /*
    * This reads a json file containing the system's configuration, i.e., the
    * partitions that are part of it. Notice that this is agnostic to how
    * partitions are constituted; the multicast agent is the one that should
    * manage the mapping of partitions to groups/rings/whatever implements the
    * partitions.
    * 
    * { "partitions" : [ {"id" : 1} , {"id" : 2} ] }
    */
   public static LocalReplica createLocalReplica(MulticastServer mcServer, String configFile) {
      LocalReplica local = new LocalReplica();
      LocalReplica.replica = local;
      local.multicastServer = mcServer;

      Partition.loadPartitions(configFile);

      int localGroupId = mcServer.getMulticastAgent().getLocalGroup().getId();
      local.localPartition = Partition.getPartition(localGroupId);

      local.start();

      return local;
   }

   /*
    * If a command C has already been delivered and executed, all necessary
    * synch with other partitions has been done already. The only exception is
    * in the case when then new delivery was done to a partition P that had not
    * delivered the command before, in which case P must be told to ignore the
    * already executed command.
    */
   public Command deliverCommandConservatively() {
      try {
         Command C = conservativeCommandsQueue.take();
         C.rewind();
         return C;
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
      return null;
   }
   
   public CommandExecution takeNextAwaitingExecution() {
      try {
         return conservativeExecutionQueue.take();
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
      return null;
   }
   
   public void markConservativeExecutionAsFinished(CommandExecution ce) {
      synchronized (pendingConservativeExecutions) {
         pendingConservativeExecutions.remove(ce);
      }
   }
   
   public Command deliverCommandOptimistically() {
      try {
         return optimisticCommandsQueue.take();
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
      return null;
   }
   
   public Command deliverCommandFast(boolean timeout, int timeoutMS) {
      try {
         if (timeout)
            return fastCommandsQueue.poll(timeoutMS, TimeUnit.MILLISECONDS);
         else
            return fastCommandsQueue.take();
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
      return null;
   }

   public Object deliverReturnValue() {
      try {
         return returnValuesQueue.take();
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
      return null;
   }
   
   public Message takeConservativeObjectDiff(CmdId cmdId, ObjId objId) {
      return receivedConsObjectDiffs.takeObjectDiff(cmdId, objId, false);
   }
   
   public Message takeOptimisticObjectDiff(CmdId cmdId, ObjId objId) {
      return receivedOptObjectDiffs.takeObjectDiff(cmdId, objId, true);
   }
   
   public boolean isPartitionMulticaster(Command cmd) {
      List<Integer> partitionMembers = localPartition.getGroup().getMembers();
      int multicasterIndex = cmd.id.clientId % partitionMembers.size();

      return this.multicastServer.getId() == partitionMembers.get(multicasterIndex);
   }

   public void start() {
      conservativeDelivererThread = new Thread(new ConservativeDeliverer(this), "ConservativeDeliverer");
      fastDelivererThread         = new Thread(new FastDeliverer(this), "OptimisticDeliverer");
      
      conservativeDelivererThread.start();
      fastDelivererThread.start();
   }
   
   boolean thisIsTheReplyingReplica(Command command) {
      List<Partition> dests = command.getDestinations();
      int clientId = command.getSourceClientId();

      int senderPartitionIndex = clientId % dests.size();
      Partition senderPartition = dests.get(senderPartitionIndex);      

      boolean thisIsTheReplyingPartition = LocalReplica.getLocalReplica().getLocalPartition() == senderPartition;
      boolean clientIsConnectedToThisReplica = multicastServer.isConnectedToClient(clientId);
      
      if (thisIsTheReplyingPartition && clientIsConnectedToThisReplica)
         return true;
      else
         return false;
   }
   
   public void sendConservativeReply(Message reply, Command command) {
      sendReply(reply, command, MessageType.REPLY_CONS_FULL);
   }
   
   public void sendOptimisticReply(Message reply, Command command) {
      sendReply(reply, command, MessageType.REPLY_OPT);
   }
   
   public void sendConservativeAckOfOptimisticReply(Command command) {
      sendReply(null, command, MessageType.REPLY_CONS_OPT_CONF);
   }
   
   public void sendReply(Message reply, Command command, MessageType type) {
      CmdId cmdId     = command.getId();
      int   clientId  = cmdId.clientId;
      long  requestId = cmdId.clientRequestSequence;

      // decide if this server should send the reply to this client
      if (thisIsTheReplyingReplica(command)) {
         Message replyWrapper = new Message(requestId, type, reply);
         replyWrapper.copyTimelineStamps(command);
         multicastServer.sendReply(clientId, replyWrapper);
      }
   }
}
