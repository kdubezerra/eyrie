package ch.usi.dslab.bezerra.eyrie;

public interface LockableMachine {
   void lockState();
   void unlockState();
   boolean isOptimistic();
}
