/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.eyrie;

import java.io.Serializable;

public class ObjId implements Serializable, Comparable<ObjId> {
   private static final long serialVersionUID = 1L;
   
   public int first;
   public int second;
   public int third;
   
   public ObjId() {}
   
   public ObjId(ObjId other) {
      this.first  = other.first;
      this.second = other.second;
      this.third  = other.third;
   }
   
   public ObjId(int first, int second, int third) {
      this.first  = first;
      this.second = second;
      this.third  = third;
//      System.out.println(this);
   }
   
   @Override
   public boolean equals(Object other_) {
      ObjId other = ((ObjId) other_);
      return this.first == other.first 
            && this.second == other.second
            && this.third  == other.third;
   }
   
   @Override
   public int hashCode() {
      return (first << 16) ^ (second << 8) ^ third;
   }
   
   @Override
   public String toString() {
      return "(" + first + "," + second + "," + third + ")";
   }

   @Override
   public int compareTo(ObjId o) {

      int d1 = this.first  - o.first;
      if (d1 != 0) return d1;

      int d2 = this.second - o.second;
      if (d2 != 0) return d2;

      int d3 = this.third  - o.third;
      if (d3 != 0) return d3;

      return 0;
      
   }
}
