/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.eyrie;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ch.usi.dslab.bezerra.eyrie.exec.OptimisticStateMachine;
import ch.usi.dslab.bezerra.netwrapper.Message;

public abstract class PRObject implements Serializable, Comparable<PRObject> {
   private static final long serialVersionUID = 1L;
   private static Map<ObjId, PRObject> conservativeObjectIndex;
   private static Map<ObjId, PRObject> optimisticObjectIndex;
   private static Map<ObjId, Partition> objectPartitioning;
   private boolean optimistic;

   
   static {
      conservativeObjectIndex = new ConcurrentHashMap<ObjId, PRObject>();
      optimisticObjectIndex   = new ConcurrentHashMap<ObjId, PRObject>();
      objectPartitioning      = new ConcurrentHashMap<ObjId, Partition>();
   }
   
   
   ObjId id;
   int ownerPartitionId;
   
   
   public static PRObject getObjectById(int id1, int id2, int id3) {
      return getObjectById(new ObjId(id1, id2, id3));
   }

   
   public static final PRObject getObjectById(ObjId id) {
      boolean isOptExecution = Command.executionIsOptimistic();
      return getObjectById(id, isOptExecution);
   }
      
      
   public static final PRObject getObjectById(ObjId id, boolean optimistic) {
      return optimistic ? optimisticObjectIndex.get(id) : conservativeObjectIndex.get(id);
   }

   
   public static void indexPRObject(PRObject obj, ObjId index) {
      obj.id = index;
      conservativeObjectIndex.put(index, obj);
      obj.optimistic = false;
//      System.out.println("\nindexing object " + index + " as conservative");
      obj.setOptimisticState(obj.deepClone());
   }
   
   
   public final boolean isOptimistic() {
      return optimistic;
   }
   
   
   public static void loadConfig(String configFile) {
      /* requires overriding by the user's subclass
       * it's not necessary for the user class to implement this
       * method, since other ways of loading objects can be used,
       * but it may be useful gto centralize the loading in a
       * single configuration file that encodes the objects somehow. 
       * 
       * e.g.: {"objects" = [ {"id" : 1, "value": 123, "partition": 1} ,
                              {"id" : 2, "value": -23, "partition": 2}
                            ]
               }
       */
   }
   
   
   public PRObject() {
//      System.out.println("PRObject() has been called");
   }
   
   
   public PRObject(int id1, int id2, int id3) {
      System.out.println("PRObject(int id1, int id2, int id3) has been called");
      PRObject.indexPRObject(this, new ObjId(id1, id2, id3));      
   }
   
   
   public PRObject(ObjId id, int owner) {
      System.out.println("PRObject(ObjId id, int owner) has been called");
      PRObject.indexPRObject(this, id);
      this.setPartition(Partition.getPartition(owner));
   }

   
   public ObjId getId() {
      return id;
   }
   
   
   public PRObjectReference getPRReference() {
      return new PRObjectReference(id);
   }

   
   public boolean isLocal() {
      Partition owner = objectPartitioning.get(this.id);
//      Partition localPartition = LocalReplica.replica.getPartition();
//      System.out.println(String.format("Partition that owns obj. %s: %s; Local partition = %s", this.id, owner, localPartition));
      return LocalReplica.replica.getLocalPartition() == owner;
   }
   
   
   public void setId(int id1, int id2, int id3) {
      if (Command.executionIsOptimistic())
         return;
      setId(new ObjId(id1, id2, id3));
      PartitioningOracle oracle = StateMachine.getMachine().getOracle();
      setPartition(oracle.getObjectPlacement(this));
   }
   
   
   public void setId(ObjId id) {
      indexPRObject(this, id);
   }
   
   
   public void unIndex() {
      conservativeObjectIndex.remove(this.id);
      optimisticObjectIndex.remove(this.id);
   }

   
   public void setPartition(Partition part) {
      objectPartitioning.put(this.id, part);
      getConservativeState().ownerPartitionId = getOptimisticState().ownerPartitionId = part.getId();
   }
   
   
   public void unsetPartition() {
      objectPartitioning.remove(this.id);
   }
   
   
   public Partition getPartition() {
      return Partition.getPartition(ownerPartitionId);
   }
   
   
   @Override
   public int compareTo(PRObject other) {
      return this.id.first - other.id.first;
   }
   
   public PRObject getOptimisticState() {
      return getObjectById(this.id, true);
   }
   
   public void setOptimisticState(PRObject newState) {
      newState.id = this.id;
      optimisticObjectIndex.put(this.id, newState);
      newState.optimistic = true;
//      System.out.println("Copied the conservative state of " + newState.id + " onto the optimistic state.");
   }
   
   public PRObject getConservativeState() {
      return getObjectById(this.id, false);
   }
   
   public void setConservativeState(PRObject newState) {
      newState.id = this.id;
      conservativeObjectIndex.put(this.id, newState);
   }
   
   public void copyConservativeStateOntoOptimisticState() {
      setOptimisticState(getConservativeState().deepClone());
   }

   public abstract Message getDiff(Partition destination);
   
   // "super" because this must return a diff that works for all
   // partitions, which may have different versions of the object
   public abstract Message getSuperDiff(List<Partition> destinations);
   
   // we assume that the user's updateFromDiff method does not change objectDiff
   // i.e., using it twice (opt and cons) is not a problem
   public void updateFromDiffSync(Message objectDiff) {
      if (OptimisticStateMachine.getMachine().exchangesOptimisticState()) {
         updateFromDiff(objectDiff);
      } else {
         Message diffCopy;
         synchronized (objectDiff) {
            diffCopy = objectDiff.deepDuplicate();
         }
         updateFromDiff(diffCopy);
      }
   }
   
   public abstract void updateFromDiff(Message objectDiff);
   
   
   public abstract PRObject deepClone();
}
