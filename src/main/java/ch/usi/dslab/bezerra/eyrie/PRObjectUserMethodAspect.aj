/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.eyrie;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import ch.usi.dslab.bezerra.eyrie.LocalReplica.MessageType;
import ch.usi.dslab.bezerra.eyrie.LocalReplica.ObjectDiffRepository;
import ch.usi.dslab.bezerra.eyrie.exec.CommandExecution;
import ch.usi.dslab.bezerra.eyrie.exec.OptimisticStateMachine;
import ch.usi.dslab.bezerra.netwrapper.Message;

public aspect PRObjectUserMethodAspect {
   
   private Method getMethod(JoinPoint joinPoint) {
      Object obj = joinPoint.getTarget();
      MethodSignature sig = (MethodSignature) joinPoint.getSignature();
      Method interceptedMethod = null;
      try {
         interceptedMethod = obj.getClass().getMethod(sig.getName(), sig.getParameterTypes());
      } catch (Exception e) {
         e.printStackTrace();
         System.exit(1);
      }
      return interceptedMethod;
   }

   pointcut sub_nonannotated() : ! execution(@LocalMethodCall * PRObject+.*(..));
   
   pointcut sub_annotated() : execution(@LocalMethodCall * PRObject+.*(..));
   
   pointcut sub() : execution(* PRObject+.*(..));

   pointcut notBase() : ! execution(* PRObject.*(..));

   pointcut cons() : execution(public PRObject+.new(..)) && ! execution(public PRObject.new(..));

   Object around() : sub_annotated() && sub() && notBase() {
      boolean executionIsOpt = Command.executionIsOptimistic();
      PRObject obj = (PRObject) thisJoinPoint.getTarget();
      
      if (obj == null) {
         // method is static; static methods must be aware of the existence of 2 (opt and cons) states
         return proceed();
      }
      
      if (executionIsOpt && obj.isOptimistic() == false) {
         Method interceptedMethod = getMethod(thisJoinPoint);
         Object[] args = thisJoinPoint.getArgs();
         PRObject optCopy = PRObject.getObjectById(obj.id, true);
         try {
//            System.out.println("Redirecting local method call to optimistic copy of " + obj.id);
            return interceptedMethod.invoke(optCopy, args);
         }
         catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            System.exit(1);
         }
      }
      // ===========================================
      // DEBUG =====================================
//      else if (executionIsOpt && obj.isOptimistic()){
//         Method interceptedMethod = getMethod(thisJoinPoint);
////         System.out.println("OSM intercepted local optimistic method: " + interceptedMethod);
//      }
//      else if (executionIsOpt == false && obj.isOptimistic() == false){
//         Method interceptedMethod = getMethod(thisJoinPoint);
////         System.out.println("CSM intercepted local conservative method: " + interceptedMethod);
//      }
//      else if (executionIsOpt == false && obj.isOptimistic()){
//         Method interceptedMethod = getMethod(thisJoinPoint);
////         System.out.println("CSM intercepted local optimistic method (WTF?!): " + interceptedMethod);
//      }
      // ===========================================
      // ===========================================
      return proceed();
   }
   
   /* advice sub class methods but not annotation or parent */
   Object around() : sub_nonannotated() && sub() && notBase() {
//      System.out.println("Intercepted non-local method.");
      // TODO: what if the object does not exist in this replica?
      // answer: for now, we assume that every object exists everywhere,
      // but not necessarily up-to-date
      
      boolean executionIsOpt = Command.executionIsOptimistic();
      PRObject obj = (PRObject) thisJoinPoint.getTarget();
      if (executionIsOpt && obj.isOptimistic() == false) {
         Method interceptedMethod = getMethod(thisJoinPoint);
         Object[] args = thisJoinPoint.getArgs();
//         System.out.println("Redirecting non-local method call to optimistic copy of " + obj.id);
         PRObject optCopy = PRObject.getObjectById(obj.id, true);
         try {
            return interceptedMethod.invoke(optCopy, args);
         }
         catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            System.exit(1);
         }
      }
      // ===========================================
      // DEBUG =====================================
//      else if (executionIsOpt && obj.isOptimistic()){
//         Method interceptedMethod = getMethod(thisJoinPoint);
//         System.out.println("OSM intercepted non-local optimistic method: " + interceptedMethod);
//      }
//      else if (executionIsOpt == false && obj.isOptimistic() == false){
//         Method interceptedMethod = getMethod(thisJoinPoint);
//         System.out.println("CSM intercepted non-local conservative method: " + interceptedMethod);
//      }
//      else if (executionIsOpt == false && obj.isOptimistic()){
//         Method interceptedMethod = getMethod(thisJoinPoint);
//         System.out.println("CSM intercepted non-local optimistic method (WTF?!): " + interceptedMethod);
//      }
      // ===========================================
      // ===========================================

      StateMachine           consSM = StateMachine.getMachine();
      OptimisticStateMachine optSM  = OptimisticStateMachine.getMachine();
      
      CommandExecution currentExecution;
      if (executionIsOpt) {
//         System.out.println("ASPECT :: Execution is optimistic ***********");
         currentExecution = optSM.currentOptimisticExecution;
      }
      else {
//         System.out.println("ASPECT :: Execution is conservative ***********");
         currentExecution = consSM.currentConservativeExecution;
      }
      
      currentExecution.markChangedObject(obj.id);
      
      Command currentCommand = currentExecution.getCommand();
      
      if (currentCommand.destinations.size() == 1)
         return proceed();
      
      Set<ObjId> exchangedObjects = currentExecution.getExchangedObjects();
//      AppServer.printAddresses("exchangedObjects address", exchangedObjects);
      
      // if running baseline, don't send anything
      
      if (exchangedObjects.contains(obj.getId()) == false) {
         
//         System.out.println(String.format("ASPECT :: exchangedObjects.contains(%s) == false", obj.getId()));
         
         exchangedObjects.add(obj.getId());
      
//         System.out.println(String.format("ASPECT :: %s.isLocal() = %b", obj.getId(), obj.isLocal()));
         
         if ( obj.isLocal() && (executionIsOpt == false || optSM.exchangesOptimisticState()) ) {
//            System.out.println(String.format("AJ(%s): Object %s is local, sending state", (executionIsOpt ? "Osm" : "Csm"), obj.id));
            
            if (LocalReplica.replica.isPartitionMulticaster(currentCommand) == false)
               return proceed();
            
            // get the set of _other_ partitions that delivered this command
            ArrayList<Partition> destinations = new ArrayList<Partition>(currentCommand.getDestinations());
            
            // TODO: if multiple partitions can own an object, then all such owners
            //       should be removed here. However, we are assuming (for now) that
            //       each object either has a single owner partition or is fully replicated
            //       not being a PRObject and its methods are not intercepted
            destinations.remove(LocalReplica.replica.localPartition);
            
            //====================================================
            // short-circuit to avoid unnecessary calls to lower (mcast) layer
            if (destinations.isEmpty()) {
               return proceed();
            }
            //====================================================
            
            // diff and send object to the other partitions
            MessageType msgType       = executionIsOpt ? LocalReplica.MessageType.OBJ_OPT : LocalReplica.MessageType.OBJ_CONS;
            Message     objectDiff    = obj.getSuperDiff(destinations);
            Message     objectMessage = new Message(msgType, currentCommand.id, obj.id, objectDiff);
            LocalReplica.replica.multicastObject(destinations, objectMessage);
            
         }
         if ( obj.isLocal() == false ) { // i.e., obj is a remote object
//            System.out.println(String.format("AJ(%s): Object %s is not local, waiting for remote state...", (executionIsOpt ? "Osm" : "Csm"), obj.id));
            
            // wait for the return value from other partitions
//            Object retval = LocalReplica.replica.deliverReturnValue();
            
            if (executionIsOpt) {
//               System.out.print(String.format("ASPECT WEAVED :: remote OPT copy of object %s awaiting its ... ", obj.id));
            }
            else {
//               System.out.print(String.format("ASPECT WEAVED :: remote CONS copy of object %s awaiting its ", obj.id));
            }
            
//            currentCommand.waitedObject = true;
//            currentCommand.t_start_waiting_object = System.currentTimeMillis();
            
            Message remoteDiff;
            if (executionIsOpt && optSM.exchangesOptimisticState()) {
//               System.out.println("OPT update");
               // TODO: return null if received an error status from a remote part
               remoteDiff = LocalReplica.replica.takeOptimisticObjectDiff(currentCommand.id, obj.id);
//               System.out.println(String.format("ASPECT WEAVED :: RECEIVED OPT update for object %s", obj.id));
            }
            else {
//               System.out.println("CONS update");
               remoteDiff = LocalReplica.replica.takeConservativeObjectDiff(currentCommand.id, obj.id);
//               System.out.println(String.format("ASPECT WEAVED :: RECEIVED CONS update for object %s", obj.id));
            }
            if (remoteDiff != ObjectDiffRepository.ERROR_DIFF)
               obj.updateFromDiffSync(remoteDiff);
            else
               optSM.startRepairing();
            
//            currentCommand.t_end_waiting_object = System.currentTimeMillis();
            
         }
      }
      else {
//         System.out.println(String.format("ASPECT :: exchangedObjects.contains(%s) == true", obj.getId()));
      }
      
      // object must be sent BEFORE method execution
      return proceed();
   }

/*
// * TODO: reenable object instanciation interception (kryo also calls new(), so we should handle that).
   // Advice subclass constructors but not Base's constructor
   Object around() : cons() {

      Command  cmd = StateMachine.currentCommand;
      if (cmd == null) {
         System.out.println("Object creation NOT intercepted.");
         return proceed();
      }
      System.out.println("Object creation intercepted.");

      PartitioningOracle oracle = StateMachine.oracle;
      
      Partition owner = oracle.getObjectPlacement(cmd);
      obj.setPartition(owner);
      
      
      System.out.println("weaved: subclass object instantiation; indexed object as a PRObject");
      System.out.println("weaved: check somehow if the object already has an id or if the aspect should create one");
      int id3 = StateMachine.currentCommandObjectCreationCount++;
      PRObject.indexPRObject(obj, new ObjId(cmd.id.first, cmd.id.second, id3));
      return proceed();
   }
*/

}