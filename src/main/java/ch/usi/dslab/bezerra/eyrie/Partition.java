/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.eyrie;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import ch.usi.dslab.bezerra.mcad.Group;

public class Partition implements Comparable<Partition> {
   public static Map<Integer, Partition> partitionList = new HashMap<Integer,Partition>();

   public static Partition getPartition(int id) {
      return partitionList.get(id);
   }
   
   public static void loadPartitions(String configFile) {
      try {
         JSONParser parser = new JSONParser();
         Object obj;

         obj = parser.parse(new FileReader(configFile));

         JSONObject partitions = (JSONObject) obj;

         JSONArray partitionArray = (JSONArray) partitions.get("partitions");

         @SuppressWarnings("unchecked")
         // Using legacy API in the next line of code
         Iterator<Object> it_part = partitionArray.iterator();

         while (it_part.hasNext()) {
            JSONObject partition = (JSONObject) it_part.next();
            long partition_id = (Long) partition.get("id");
            new Partition((int) partition_id);
         }

      } catch (IOException | ParseException e) {
         e.printStackTrace();
      }
   }
   
   public static List<Partition> getPartitionsList() {
      List<Partition> partList = new ArrayList<Partition>(partitionList.values());
      Collections.sort(partList, new Comparator<Partition>() {
         public int compare(Partition o1, Partition o2) {
            return o1.getId() - o2.getId();
         }
      });
      return partList; 
   }
   
   public static int getPartitionsCount() {
      return partitionList.size();
   }
   
   int id;
   Group pgroup;

   public Partition(int id) {
      this.id = id;
      pgroup = Group.getOrCreateGroup(id); // assuming partition's and group's ids will match
      assert(pgroup != null);
      partitionList.put(id, this);
   }
   
   public int getId() {
      return id;
   }

   public Group getGroup() {
      return pgroup;
   }
   
   @Override
   public boolean equals(Object Oother) {
      Partition other = (Partition) Oother;
      return this.id == other.id;
   }
   
   @Override
   public int hashCode() {
      return id;
   }

   @Override
   public int compareTo(Partition other) {
      return this.pgroup.getId() - other.pgroup.getId();
   }
   
   @Override
   public String toString() {
      return String.format("P_%d", id);
   }

}
