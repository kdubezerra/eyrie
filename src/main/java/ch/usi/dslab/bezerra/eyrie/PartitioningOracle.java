/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.eyrie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import ch.usi.dslab.bezerra.eyrie.cs.CallbackHandler;

public class PartitioningOracle {
   
   public static class Prophecy {
      public List<Partition> destinations;
      public Object context;
      public boolean async;
      public boolean complete;
      public Prophecy() {
         
      }
      public Prophecy(List<Partition> destinations, Object context) {
         this.destinations = destinations;
         this.context      = context;
         this.async        = false;
         this.complete     = true;
      }
      public static Prophecy newAsync() {
         Prophecy ap = new Prophecy();
         ap.async = true;
         return ap;
      }
   }
   
   final Prophecy getCleanProphecy(Command cmd) {
      Prophecy prophecy = getProphecy(cmd);
      prophecy.destinations = new ArrayList<Partition>(new HashSet<Partition>(prophecy.destinations));
      Collections.sort(prophecy.destinations);
      return prophecy;
   }
   
   public Prophecy getProphecy(Command cmd) {
      return new Prophecy(Partition.getPartitionsList(), null);
   }
   
   public Prophecy getProphecyForNonBlocking(Command cmd) {
      return new Prophecy(Partition.getPartitionsList(), null);
   }
   
   public CallbackHandler getProphecyHandler() {
      return null;
   }
   
   public Partition getObjectPlacement(Command cmd) {
      Random rand = new Random();
      List<Partition> parts = Partition.getPartitionsList();
      int numParts = parts.size();
      return parts.get(rand.nextInt() % numParts);
   }
   
   public Partition getObjectPlacement(PRObject obj) {
      Random rand = new Random();
      List<Partition> parts = Partition.getPartitionsList();
      int numParts = parts.size();
      int chosenPartition = rand.nextInt(numParts);
//      System.out.println(String.format("Partition list: %s, chosenPartition: %d", parts, chosenPartition));
      return parts.get(chosenPartition);
   }
   
   // assemble a list of objects to be sent to destinations
   public List<ObjId> getEarlyObjectsIds(Command cmd) {
      return null;
   }
   
   public boolean knowsAllEarlyObjectIds(Command cmd) {
      return false;
   }

}
