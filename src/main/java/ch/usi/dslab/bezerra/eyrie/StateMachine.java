/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.eyrie;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ch.usi.dslab.bezerra.eyrie.CmdId;
import ch.usi.dslab.bezerra.eyrie.Command;
import ch.usi.dslab.bezerra.eyrie.LocalReplica;
import ch.usi.dslab.bezerra.eyrie.Partition;
import ch.usi.dslab.bezerra.eyrie.PartitioningOracle;
import ch.usi.dslab.bezerra.eyrie.exec.CommandExecution;
import ch.usi.dslab.bezerra.eyrie.exec.CommandExecution.Signal;
import ch.usi.dslab.bezerra.eyrie.exec.OptimisticStateMachine;
import ch.usi.dslab.bezerra.mcad.MulticastClientServerFactory;
import ch.usi.dslab.bezerra.mcad.MulticastServer;
import ch.usi.dslab.bezerra.netwrapper.Message;

public abstract class StateMachine implements LockableMachine {

   private static StateMachine instance;
   
//   static Command currentCommand = null;
   // the following is the set of all objects sent/received during the execution
   // of the current command
   // a remote object doesn't need to be sent/received twice during the
   // excecution of the same command
   CommandExecution currentConservativeExecution;
//   static List<ObjId> currentCommandExchangedObjects = new ArrayList<ObjId>();
//   static int currentCommandObjectCreationCount;
//   static List<Partition> currentCommandPartitionsWaitingForSignals;
//   static List<Signal> currentCommandSignalsYetToReceive;
   List<Signal> signalQueue = new ArrayList<Signal>();

   private final Lock signalsLock = new ReentrantLock();
   private final Condition signalsCondition = getSignalsLock().newCondition();

   public PartitioningOracle oracle = null;

   public List<Signal> getSignalQueue() {
      return signalQueue;
   }  
   
   private MulticastServer multicastServer;
   private OptimisticStateMachine optimisticStateMachine;
   private Lock stateLock;

   boolean running = true;
   Semaphore readySemaphore;

   public static StateMachine getMachine() {
      return instance;
   }
   
   public StateMachine(int serverId, int partitionId, String systemConfig, String partitionsConfig,
         PartitioningOracle oracle, boolean exchangesOptimisticState) {
      instance = this;
      this.oracle = oracle;
      this.readySemaphore = new Semaphore(0);
      multicastServer = MulticastClientServerFactory.getServer(serverId, systemConfig);
      LocalReplica.createLocalReplica(multicastServer, partitionsConfig);
      stateLock = new ReentrantLock();
      optimisticStateMachine = OptimisticStateMachine.createMachine(this, exchangesOptimisticState);
   }
   
   public MulticastServer getMulticastServer() {
      return multicastServer;
   }

   public void setOracle(PartitioningOracle oracle) {
      this.oracle = oracle;
   }

   public PartitioningOracle getOracle() {
      return this.oracle;
   }

   void enqueueSignal(Signal signal) {
      getSignalsLock().lock();
      {
         signalQueue.add(signal);
         getSignalsCondition().signalAll();
      }
      getSignalsLock().unlock();
   }
   
   void enqueueSignal(Message signalMessage) {
      signalMessage.rewind();
      signalMessage.getNext(); // skip message type
      int sourceId = (Integer) signalMessage.getNext();
      CmdId cmdId = (CmdId) signalMessage.getNext();

      Signal signal = new Signal(sourceId, cmdId);
      
      enqueueSignal(signal);
   }

   public void sendSignals(Command cmd) {
      LocalReplica local = LocalReplica.replica;
      if (local.isPartitionMulticaster(cmd) == false)
         return;

      List<Partition> dests = cmd.getDestinations();
      if (dests.size() <= 1)
         return;

      List<Partition> otherPartitions = new ArrayList<Partition>(dests);
      otherPartitions.remove(local.localPartition);

      Message signal = new Message(LocalReplica.MessageType.SIGNAL, local.localPartition.id, cmd.id);
      local.multicast(otherPartitions, signal);
   }
   
   List<Signal> partitionListToSignalList(List<Partition> partitions, Command cmd) {
      List<Signal> slist = new ArrayList<Signal>(partitions.size());
      for (Partition p : partitions)
         slist.add(new Signal(p.id, cmd.id));      
      return slist;
   }
   
   public Semaphore getReadySemaphore() {
      return readySemaphore;
   }

   public void runStateMachine() {
      optimisticStateMachine.startRunning();
      LocalReplica local = LocalReplica.getLocalReplica();
      readySemaphore.release();
      while (running) {

         // =========================
         // NO super-early state sent
//       Command C = local.deliverCommandConservatively();
         // =========================
         // super-early state sent: YES
         CommandExecution cmdExecution = local.takeNextAwaitingExecution();
         Command currentCommand = cmdExecution.getCommand();
         // =========================

         //================
         // TIMELINE STUFF
         currentCommand.t_command_dequeued = System.currentTimeMillis();
         //================


         // lock the state to prevent the optimistic state-machine from
         // copying the conservative state in the middle of an execution
         lockState();

         // =========================
         // NO super-early state sent
//       currentConservativeExecution = new CommandExecution(C, local);
         // =========================
         // super-early state sent: YES
         currentConservativeExecution = cmdExecution;
         // =========================

         LocalReplica.replica.multicastLocalEarlyObjects(currentConservativeExecution, false);

         // measurement, non-essential
//         C.t_execution_start = System.currentTimeMillis();
         //

         // inside the executeCommand(...) method, there used to be a simple implementation
         // of the user's server logic. with optimistic execution, though, such method first
         // checks if the command was already opt-executed, if its order coincides
         // with the optimistic order and confirm/rectify the execution (and the reply accordingly).
         //
         // if the execution is confirmed, *reply* is simply a "confirm opt execution"
         // otherwise, *reply* contains the reply that should've been sent originally
         Message reply = executeCommandConservatively(currentCommand);
         currentConservativeExecution.setReply(reply);

         // let the OSM know about the conservative execution order
         optimisticStateMachine.appendConservativeExecution(currentConservativeExecution);
         unlockState();

         // =========================
         // NO super-early state sent
         // =========================
         // super-early state sent: YES
         local.markConservativeExecutionAsFinished(currentConservativeExecution);
         // =========================
         
         // before proceeding to the next command (and maybe before
         // even replying to the client) wait until all signals have
         // been sent/received
         currentConservativeExecution.waitPendingSignals();
         
         // verifying the optimistic execution against this conservative execution
         if (optimisticStateMachine.confirmLocalOptimisticExecution(currentConservativeExecution) == true) {
            // send just an ACK to the client, confirming the optimistic reply sent previously
            local.sendConservativeAckOfOptimisticReply(currentCommand);
            
//            C.t_server_send = System.currentTimeMillis();
//            reply.copyTimelineStamps(C);
//            local.sendConservativeReply(reply, C);
         }
         else {
            // send the reply to the client
//            C.t_server_send = System.currentTimeMillis();
//            reply.copyTimelineStamps(C);
            local.sendConservativeReply(reply, currentCommand);
         }


         // clear list of received objects
         if (currentCommand.destinations.size() > 1)
            local.notifyDoneWithConsDiff(currentCommand.id);

         // garbage collect signals for old commands
         // gargabeCollectSignals();
      }
   }
   
   @Override
   public void lockState() {
      stateLock.lock();
   }
   
   @Override
   public void unlockState() {
      stateLock.unlock();
   }
   
   public Lock getSignalsLock() {
      return signalsLock;
   }

   public Condition getSignalsCondition() {
      return signalsCondition;
   }

   public Message executeCommandConservatively(Command cmd) {
      // TODO: fix this, i.e., execute the command against the conservative state and check if
      //       the fast execution was already done in the correct order
      //       - if yes, just send a confirmation to the client
      //       - if not, re-execute the command
      //
      // TODO: - also, check if the exchanged objects were confirmed by their owner partitions
      cmd.rewind();
      Message conservativeReply = executeCommand(cmd);
//      System.out.println("Conservative reply to command " + cmd.id + ": " + conservativeReply);
      return conservativeReply;
   }
   
   @Override
   public final boolean isOptimistic() {
      return false;
   }

   // executeCommand returns the reply to be sent to the client
   public abstract Message executeCommand(Command cmd);
}
