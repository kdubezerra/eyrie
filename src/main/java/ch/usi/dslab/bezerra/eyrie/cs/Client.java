/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.eyrie.cs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicLong;

import ch.usi.dslab.bezerra.eyrie.Command;
import ch.usi.dslab.bezerra.eyrie.LocalReplica;
import ch.usi.dslab.bezerra.eyrie.LocalReplica.MessageType;
import ch.usi.dslab.bezerra.eyrie.Partition;
import ch.usi.dslab.bezerra.eyrie.PartitioningOracle;
import ch.usi.dslab.bezerra.eyrie.PartitioningOracle.Prophecy;
import ch.usi.dslab.bezerra.mcad.MulticastClient;
import ch.usi.dslab.bezerra.mcad.MulticastClientServerFactory;
import ch.usi.dslab.bezerra.mcad.Group;
import ch.usi.dslab.bezerra.netwrapper.Message;

public class Client implements Runnable {

   public static class OutstandingRequest {
      CallbackHandler optimisticHandler;
      CallbackHandler conservativeHandler;
      Object  context;
      Message optimisticReply;
      public OutstandingRequest(CallbackHandler optimisticHandler, CallbackHandler conservativeHandler, Object ctx) {
         this.optimisticHandler   = optimisticHandler;
         this.conservativeHandler = conservativeHandler;
         context = ctx;
      }
      public void saveOptimisticReply(Message reply) {
         optimisticReply = reply.duplicate();
         optimisticReply.rewind();
      }
      public Message getOptimisticReply() {
         return optimisticReply;
      }
      public boolean hasOptimisticHandler() {
         return optimisticHandler != null;
      }
   }
   
   public static class Callback implements Runnable {
      CallbackHandler handler;
      Object          context;
      Object          reply;
      
      public Callback(CallbackHandler handler, Object context, Object reply) {
         this.handler = handler;
         this.context = context;
         this.reply   = reply;
      }

      @Override
      public void run() {
         handler.handleCallback(reply, context);
      }
      
   }

   PartitioningOracle oracle;
   MulticastClient    multicastClient;
   Thread             clientThread;

   int  clientId;
   AtomicLong nextRequestId = new AtomicLong(0);

   BlockingQueue<Message> syncDeliveredConservativeReplies = new LinkedBlockingDeque<Message>();
   
   Map<Long, OutstandingRequest> outstandingRequests  = new ConcurrentHashMap<Long, OutstandingRequest>();
   ExecutorService callbackExecutor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
   Map<Long, Message> outOfOrderConservative = new HashMap<Long, Message>();
   
   boolean running = true;
   
   public Client (int clientId, String systemConfigFile, String partitioningFile, PartitioningOracle oracle) {
      this.clientId = clientId;
      this.oracle   = oracle;
      multicastClient = MulticastClientServerFactory.getClient(clientId, systemConfigFile);
      Partition.loadPartitions(partitioningFile);
      for (Partition p : Partition.partitionList.values())
         connectToPartition(p);
      clientThread = new Thread(this, "EyrieClientThread");
      clientThread.start();
   }
   
   public int getId() {
      return clientId;
   }
   
   private void connectToPartition(Partition partition) {
      List<Integer> partitionServers = partition.getGroup().getMembers();
      int contactServerIndex = clientId % partitionServers.size();
      int contactServer = partitionServers.get(contactServerIndex);
      multicastClient.connectToServer(contactServer);
   }
   
   private void sendCommand(long requestId, Command command) {
      command.setId(clientId, requestId);
      Prophecy prophecy = oracle.getProphecy(command);
      command.setDestinations(prophecy.destinations);
      List<Group> destinationGroups = new ArrayList<Group>(prophecy.destinations.size());
      for (Partition partition : prophecy.destinations)
         destinationGroups.add(partition.getGroup());
//      command.t_client_send = System.currentTimeMillis();
      
//      // if the prophecy has already the complete set of destinations to work with,
//      // just send the command to such destinations
//      if (prophecy.complete) {

         Message commandMessage = new Message(LocalReplica.MessageType.COMMAND, command);
         multicastClient.multicast(destinationGroups, commandMessage);
      
//      }
//      // however, if the prophecy requires a first interaction with the service, send a
//      // Prophecy Query first.
//      else {
//         
//         Message prophecyQuery = new Message(LocalReplica.MessageType.COMMAND, prophecy.context);
//         multicastClient.multicast(destinationGroups, prophecyQuery);
//         
//      }
   }
   
   private Message deliverSync() {
      Message reply = null;
      try {
         reply = syncDeliveredConservativeReplies.take();
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
      return reply;
   }

   public Message sendCommandSync(Command command) {
      sendCommand(nextRequestId.incrementAndGet(), command);
      Message reply = deliverSync();
      return reply;
   }

   public void sendCommandAsync(CallbackHandler callbackHandler, Object context, Command command) {
      sendCommandAsync(null, callbackHandler, context, command);
   }
   
   public void sendCommandAsync(CallbackHandler optimisticCBHandler, CallbackHandler conservativeCBHandler, Object context, Command command) {
      long requestId = nextRequestId.incrementAndGet();
      OutstandingRequest osr = new OutstandingRequest(optimisticCBHandler, conservativeCBHandler, context);
      outstandingRequests.put(requestId, osr);
      sendCommand(requestId, command);
   }

   public void setOracle(PartitioningOracle oracle) {
      this.oracle = oracle;
   }
   
   @Override
   public void run() {
      while (running) {
         Message     replyWrapper = multicastClient.deliverReply();
         long        reqId        = (Long) replyWrapper.getItem(0);
         MessageType type         = (MessageType) replyWrapper.getItem(1);
         Message     reply        = (Message) replyWrapper.getItem(2);
         
         switch (type) {
            case REPLY_OPT : {
               OutstandingRequest osreq = outstandingRequests.get(reqId);
               boolean optReceivedFirstAndNonSyncRequest = (osreq != null);
               
               if (optReceivedFirstAndNonSyncRequest) {
                  if (outOfOrderConservative.containsKey(reqId)) {
                     Message outOfOrderConservativeAck = outOfOrderConservative.get(reqId);
                     reply.copyTimelineStamps(outOfOrderConservativeAck);
                     callbackExecutor.execute(new Callback(osreq.conservativeHandler, osreq.context, reply));
                     outstandingRequests.remove(reqId);
                     outOfOrderConservative.remove(reqId);
//                     System.out.println("used optimistic reply's payload as conservative reply: confirmation was received out of order");
                  }
                  else {
                     reply.copyTimelineStamps(replyWrapper);
                     if (osreq.hasOptimisticHandler())
                        callbackExecutor.execute(new Callback(osreq.optimisticHandler, osreq.context, reply));
                     osreq.saveOptimisticReply(reply);
//                     System.out.println("saved optimistic reply to be potentially confirmed");
                  }
               }
               else {
//                  System.out.println("optimistic reply received late (or sync request). ignoring it.");
               }
               break;
            }
            
            case REPLY_CONS_FULL : {
               reply.copyTimelineStamps(replyWrapper);
               OutstandingRequest osreq = outstandingRequests.remove(reqId);
               if (osreq != null) {
                  callbackExecutor.execute(new Callback(osreq.conservativeHandler, osreq.context, reply));
//                  System.out.println("used full conservative reply");
               }
               else {
                  syncDeliveredConservativeReplies.add(reply);
               }
               break;               
            }
            
            case REPLY_CONS_OPT_CONF : {
               OutstandingRequest osreq = outstandingRequests.remove(reqId);
               Message storedReply = osreq.getOptimisticReply();
               if (storedReply == null) {
                  outOfOrderConservative.put(reqId, replyWrapper);
               }
               else {
                  storedReply.copyTimelineStamps(replyWrapper);
                  callbackExecutor.execute(new Callback(osreq.conservativeHandler, osreq.context, storedReply));
               }
               break;
            }
            
            default: {
               break;
            }
         }
         
      }
   }

}
