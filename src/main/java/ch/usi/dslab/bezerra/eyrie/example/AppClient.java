package ch.usi.dslab.bezerra.eyrie.example;


import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

import ch.usi.dslab.bezerra.eyrie.Command;
import ch.usi.dslab.bezerra.eyrie.ObjId;
import ch.usi.dslab.bezerra.eyrie.PartitioningOracle;
import ch.usi.dslab.bezerra.eyrie.cs.CallbackHandler;
import ch.usi.dslab.bezerra.eyrie.cs.Client;
import ch.usi.dslab.bezerra.netwrapper.Message;

public class AppClient {
   
   Client eyrieClient;
   private Semaphore sendPermits;
   
   public AppClient(int clientId, String systemConfigFile, String partitioningFile, PartitioningOracle oracle) {
      eyrieClient = new Client(clientId, systemConfigFile, partitioningFile, oracle);
   }
   
   public static class CallBackContext {
      Command cmd;
      long sendTimeMs;
      public CallBackContext(Command cmd, long sendTimeNano) {
         this.cmd = cmd;
         this.sendTimeMs = sendTimeNano;
      }
   }
   
   public static String getTimeLine(Message reply, long sendTimeMs) {
      long t_mcast = reply.t_learner_delivered  - sendTimeMs;
      long t_wait  = reply.t_command_dequeued   - reply.t_learner_delivered;
      long t_exec  = System.currentTimeMillis() - reply.t_command_dequeued;
      return String.format("mc: %d, w: %d, x: %d", t_mcast, t_wait, t_exec);
//      reply.t_client_receive = System.currentTimeMillis();
//      long send_reply = reply.t_client_receive   - reply.t_server_send;
//      long execution  = reply.t_server_send      - reply.t_execution_start;
//      long preexec    = reply.t_execution_start  - reply.t_command_dequeued;
//      long enqueued   = reply.t_command_dequeued - reply.t_command_enqueued;
//      long multicast  = reply.t_command_enqueued - reply.t_client_send;
//      long TOTAL      = multicast + enqueued + preexec + execution + send_reply;
      
//      String objectWaitTime = "";
//      if (reply.waitedObject)
//         objectWaitTime = String.format("(ow %d) ", reply.t_end_waiting_object - reply.t_start_waiting_object);
      
//      String timelineString = String.format("mc %d | q %d | px %d | x %d %s| r %d | T %d",
//            multicast, enqueued, preexec, execution, objectWaitTime, send_reply, TOTAL);
      
//      return timelineString;
   }
   
   
   public void sendCommand(int oid1, byte op, int oid2) {
      Command cmd = new Command(new ObjId(oid1, 0, 0), op, new ObjId(oid2, 0, 0));
      
      System.out.println(String.format("client sending command %d %s %d", oid1, (op == 0 ? "a" : (op == 1 ? "s" : "m")), oid2));
      
//    send request and block waiting for the (conservative) reply:  
//    Message reply = eyrieClient.sendCommandSync(cmd);
//    System.out.println(String.format("New value for obj%d: %d", oid1, (Integer) reply.getNext()));
      
      CallbackHandler optReplyHandler = new CallbackHandler() {
         @Override
         public void handleCallback(Object reply, Object context) {
            CallBackContext cbContext = (CallBackContext) context;
            Command cmd = cbContext.cmd;
            long sendTimeMs = cbContext.sendTimeMs;
            System.out.println(String.format("optimistic reply for command %s: %d", cmd.getId(), (Integer) ((Message)reply).getNext()));
            System.out.println("Opt. latency timeline: " + getTimeLine((Message) reply, sendTimeMs));
         }
      };
      
//      final AppClient cli = this;
      
      CallbackHandler consReplyHandler = new CallbackHandler() {
         @Override
         public void handleCallback(Object reply, Object context) {
            addPermit();
            CallBackContext cbContext = (CallBackContext) context;
            Command cmd = cbContext.cmd;
            long sendTimeMs = cbContext.sendTimeMs;
            System.out.println(String.format("conservative reply for command %s: %d", cmd.getId(), (Integer) ((Message)reply).getNext()));
            System.out.println("Cons. latency timeline: " + getTimeLine((Message) reply, sendTimeMs));
         }
      };
      
      eyrieClient.sendCommandAsync(optReplyHandler, consReplyHandler, new CallBackContext(cmd, System.currentTimeMillis()), cmd);
   }
   
   void setPermits(int num) {
      sendPermits = new Semaphore(num);
   }
   
   void addPermit() {
      if (sendPermits != null)
         sendPermits.release();
   }
   
   void getPermit() {
      try {
         sendPermits.acquire();
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }
   
   public void sendCommandBurst(int burstlength, boolean allowMultipartition) {
      sendCommandBurst(burstlength, burstlength, allowMultipartition);
   }
   
   public void sendCommandBurst(int burstlength, int outstanding, boolean allowMultipartition) {
      setPermits(outstanding);
      Random rand = new Random(System.currentTimeMillis());
      for (int i = 0; i < burstlength ; i++) {
         int oid1 = allowMultipartition ? rand.nextInt(10) : rand.nextInt(5) * 2;
         int oid2 = allowMultipartition ? rand.nextInt(10) : rand.nextInt(5) * 2;
         byte op = (byte) rand.nextInt(3);
         getPermit();
         sendCommand(oid1, op, oid2);
      }
   }
   
   public void runInteractive(){
      Scanner scan = new Scanner(System.in);
      String  input;

      System.out.println("input format: oid1 a(dd)/s(ub)/m(ul)/d(iv) oid2 (or b/l to send a command multi-partition/local burst, or END to finish)");
      input = scan.nextLine();
      while (input.equalsIgnoreCase("end") == false) {
         
         if (input.contains("b")) {
            String[] params = input.split(" ");
            int burstlength = params.length > 1 ? Integer.parseInt(params[1]) : (1+(new Random(System.currentTimeMillis())).nextInt(100));
            int outstanding = params.length > 2 ? Integer.parseInt(params[2]) : burstlength;
            sendCommandBurst(burstlength, outstanding, true);
         }
         else if (input.contains("l")) {
            String[] params = input.split(" ");
            int burstlength = params.length > 1 ? Integer.parseInt(params[1]) : (1+(new Random(System.currentTimeMillis())).nextInt(100));
            int outstanding = params.length > 2 ? Integer.parseInt(params[2]) : burstlength;
            sendCommandBurst(burstlength, outstanding, false);
         }
         else {
            String[] params = input.split(" ");
            int oid1 = Integer.parseInt(params[0]);
            int oid2 = Integer.parseInt(params[2]);
            String opStr = params[1];
   
            byte op = 0;
            if      (opStr.equalsIgnoreCase("a")) op = AppServer.ADD;
            else if (opStr.equalsIgnoreCase("s")) op = AppServer.SUB;
            else if (opStr.equalsIgnoreCase("m")) op = AppServer.MUL;
            else if (opStr.equalsIgnoreCase("d")) op = AppServer.DIV;
            
            sendCommand(oid1, op, oid2);
         }
         
         input = scan.nextLine();
      }
      scan.close();
   }
   
   public static void main(String[] args) {
      int clientId = Integer.parseInt(args[0]);
      
//    example system config file: /Users/eduardo/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/ridge/configs/example_config.json
//    String systemConfigFile = "/Users/eduardo/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/ridge/configs/example_config.json";
      String  systemConfigFile = args[1];

//    example partition config file: /Users/eduardo/eyrie/src/main/java/ch/usi/dslab/bezerra/eyrie/example/example_partitioning.json
//    String partitionsConfigFile = "/Users/eduardo/eyrie/src/main/java/ch/usi/dslab/bezerra/eyrie/example/example_partitioning.json";
      String  partitionsConfigFile = args[2];
    
      AppClient appcli = new AppClient(clientId, systemConfigFile, partitionsConfigFile, new AppOracle());
      appcli.runInteractive();
   }
   
}
