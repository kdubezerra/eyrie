package ch.usi.dslab.bezerra.eyrie.example;

import java.util.List;

import ch.usi.dslab.bezerra.eyrie.LocalMethodCall;
import ch.usi.dslab.bezerra.eyrie.PRObject;
import ch.usi.dslab.bezerra.eyrie.PRObjectReference;
import ch.usi.dslab.bezerra.eyrie.Partition;
import ch.usi.dslab.bezerra.netwrapper.Message;

public class AppObject extends PRObject {
   private static final long serialVersionUID = 1l;
   
   // any reference to another probject must be held by a probjref kind of object,
   // so that the referred object does not get serialized unnecessarily
   PRObjectReference someOtherPRObject;
   
   int value;

   public AppObject(int initialValue) {
      value = initialValue;
   }
   
   @Override
   public Message getSuperDiff(List<Partition> destinations) {
      Message diff = new Message(someOtherPRObject, value);
      return diff;
   }

   @Override
   public void updateFromDiff(Message objectDiff) {
      Message diff = objectDiff.duplicate();
      diff.rewind();
      PRObjectReference pror  = (PRObjectReference) diff.getNext();
      int               value = (Integer)           diff.getNext();
      
      this.someOtherPRObject = pror;
      this.value             = value;
   }
   
//   @LocalMethodCall
   int getValue() {
      return value;
   }
   
//   @LocalMethodCall
   void add(int value) {
      this.value += value;
   }
   
//   @LocalMethodCall
   void sub(int value) {
      this.value -= value;
   }
   
//   @LocalMethodCall
   void mul(int value) {
      this.value *= value;
   }
   
//   @LocalMethodCall
   void div(int value) {
      this.value /= value;
   }
   
   @LocalMethodCall
   void localNOP() {
   }

   @Override
   public PRObject deepClone() {
      AppObject clone = new AppObject(this.value);
      // TODO: what about the reference?
      return clone;
   }

}
