package ch.usi.dslab.bezerra.eyrie.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ch.usi.dslab.bezerra.eyrie.Command;
import ch.usi.dslab.bezerra.eyrie.ObjId;
import ch.usi.dslab.bezerra.eyrie.PRObject;
import ch.usi.dslab.bezerra.eyrie.Partition;
import ch.usi.dslab.bezerra.eyrie.PartitioningOracle;

public class AppOracle extends PartitioningOracle {

   // Command format : | ObjId o1 | int op | ObjId o2 |
   
   @Override
   public Prophecy getProphecy(Command command) {

      ObjId oi1 = (ObjId) command.getItem(0);
      ObjId oi2 = (ObjId) command.getItem(2);
      
      Set<Partition> partitionSet = new HashSet<Partition>();
      partitionSet.add(getObjectPartition(oi1));
      partitionSet.add(getObjectPartition(oi2));
      List<Partition> partitions = new ArrayList<Partition>(partitionSet);
      Collections.sort(partitions);
      
      return new Prophecy(new ArrayList<Partition>(partitions), null);
   }
   
   public Partition getObjectPartition(ObjId oid) {
      List<Partition> parts = Partition.getPartitionsList();
      int numParts = parts.size();
      int chosenPartitionId = oid.hashCode() % numParts;
      return parts.get(chosenPartitionId);
   }
   
   @Override
   public Partition getObjectPlacement(PRObject obj) {
      return getObjectPartition(obj.getId());
   }
   
   @Override
   // assemble a list of objects to be sent to destinations
   public List<ObjId> getEarlyObjectsIds(Command command) {
      ObjId oi1 = (ObjId) command.getItem(0);
      ObjId oi2 = (ObjId) command.getItem(2);
      
      Set<ObjId> objset = new HashSet<ObjId>();
      objset.add(oi1);
      objset.add(oi2);
      List<ObjId> objs = new ArrayList<ObjId>(objset);
      Collections.sort(objs);
      
      return objs;
   }
   
}
