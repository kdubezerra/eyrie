package ch.usi.dslab.bezerra.eyrie.example;

import java.lang.reflect.Field;

import sun.misc.Unsafe;

import ch.usi.dslab.bezerra.eyrie.Command;
import ch.usi.dslab.bezerra.eyrie.ObjId;
import ch.usi.dslab.bezerra.eyrie.PRObject;
import ch.usi.dslab.bezerra.eyrie.PartitioningOracle;
import ch.usi.dslab.bezerra.eyrie.StateMachine;
import ch.usi.dslab.bezerra.netwrapper.Message;

public class AppServer extends StateMachine {

   public static final byte ADD = 0;
   public static final byte SUB = 1;
   public static final byte MUL = 2;
   public static final byte DIV = 3;
   
   static final boolean is64bit = true;
   
   private static Unsafe getUnsafe() {
      try {
         Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
         theUnsafe.setAccessible(true);
         return (Unsafe) theUnsafe.get(null);
      } catch (Exception e) {
         throw new AssertionError(e);
      }
   }
   
   @SuppressWarnings("deprecation")
   public static void printAddresses(String label, Object... objects) {
      Unsafe unsafe = getUnsafe();
      
      System.out.print(label + ": 0x");
      long last = 0;
      int offset = unsafe.arrayBaseOffset(objects.getClass());
      int scale = unsafe.arrayIndexScale(objects.getClass());
      switch (scale) {
         case 4:
            long factor = is64bit ? 8 : 1;
            final long i1 = (unsafe.getInt(objects, offset) & 0xFFFFFFFFL) * factor;
            System.out.print(Long.toHexString(i1));
            last = i1;
            for (int i = 1; i < objects.length; i++) {
               final long i2 = (unsafe.getInt(objects, offset + i * 4) & 0xFFFFFFFFL) * factor;
               if (i2 > last)
                  System.out.print(", +" + Long.toHexString(i2 - last));
               else
                  System.out.print(", -" + Long.toHexString(last - i2));
               last = i2;
            }
         break;
         case 8:
            throw new AssertionError("Not supported");
      }
      System.out.println();
   }
   
   public AppServer(int serverId, int partitionId, String systemConfig, String partitionsConfig, PartitioningOracle oracle, boolean exchangesOptimisticState) {
      super(serverId, partitionId, systemConfig, partitionsConfig, oracle, exchangesOptimisticState);
   }
   
   @Override
   public Message executeCommand(Command cmd) {
      
      // Command format : | ObjId o1 | int op | ObjId o2 |
      
      ObjId oi1 = (ObjId) cmd.getNext();
      byte   op = (Byte ) cmd.getNext();
      ObjId oi2 = (ObjId) cmd.getNext();
      
//      if (Command.executionIsOptimistic()) {
//         System.out.println(String.format("XXX :: OPT-EXECUTING command %s: (%s,%d,%s)", cmd.id, oi1, op, oi2));
//      }
//      else {
//         System.out.println(String.format("XXX :: CONS-EXECUTING command %s: (%s,%d,%s)", cmd.id, oi1, op, oi2));
//      }
      
      AppObject ao1 = (AppObject) PRObject.getObjectById(oi1);
      AppObject ao2 = (AppObject) PRObject.getObjectById(oi2);
//      int       v1  = ao1.getValue();
      int       v2  = ao2.getValue();
      int     rand  = (int) (cmd.id.clientId + cmd.id.clientRequestSequence);
      switch (op) {
         case ADD : {
//            System.out.println(String.format("o%d = %d + (%d + %d) = %d", oi1.first, v1, v2, rand, v1+(v2+rand)));
            ao1.add(v2 + rand);
            break;
         }
         case SUB : {
//            System.out.println(String.format("o%d = %d - (%d + %d) = %d", oi1.first, v1, v2, rand, v1-(v2+rand)));
            ao1.sub(v2 + rand);
            break;
         }
         case MUL : {
//            System.out.println(String.format("o%d = %d * (%d + %d) = %d", oi1.first, v1, v2, rand, v1*(v2+rand)));
            ao1.mul(v2 + rand);
            break;
         }
         case DIV : {
//            System.out.println(String.format("o%d = %d / (%d + %d) = %d", oi1.first, v1, v2, rand, v1/(v2+rand)));
            ao1.div(v2 + rand);
            break;
         }
         default : {
            break;
         }
      }

      if (Command.executionIsOptimistic()) {
//         System.out.println(String.format("new OPT value for object %s: %d", ao1.getId(), ao1.getValue()));
//         printAddresses("optimistic object's address", ao1);
      }
      else {
//         System.out.println(String.format("new CONS value for object %s: %d", ao1.getId(), ao1.getValue()));
//         printAddresses("conservative object's address", ao1);         
      }

      
      Message reply = new Message(ao1.getValue());
      return reply;
   }
   
   public void createInitialObjects() {
//      List<Partition> partitionList = Partition.getPartitionsList();
      for (int i = 0 ; i < 10 ; i++) {
         AppObject obj = new AppObject(i);
         obj.setId(i, 0, 0);
      }
   }
   
   public static void main(String[] args) {
      
      int     serverId = Integer.parseInt(args[0]);
      int     partitionId = Integer.parseInt(args[1]);

//    example system config file: /Users/eduardo/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/ridge/configs/example_config.json
//    String systemConfigFile = "/Users/eduardo/libmcad/src/main/java/ch/usi/dslab/bezerra/mcad/ridge/configs/example_config.json";
      String  systemConfigFile = args[2];

//    example partition config file: /Users/eduardo/eyrie/src/main/java/ch/usi/dslab/bezerra/eyrie/example/example_partitioning.json
//    String partitionsConfigFile = "/Users/eduardo/eyrie/src/main/java/ch/usi/dslab/bezerra/eyrie/example/example_partitioning.json";
      String  partitionsConfigFile = args[3];
      
      int bitExchangesOptState = Integer.parseInt(args[4]);
      boolean exchangesOptimisticState = (bitExchangesOptState != 0);
      
      // the partitionConfig file tells which server is the one responsible for multicast objects and
      // signals in each partitions.
      // alternatively, servers could take turns to multicast objects and signals...
      // in fact, the second approach wouldn't need an assigned multicaster at all.
      // and the first one could be implemente by selection a default server (e.g., the one with lowest id in
      // the partition) to be the multicaster
      AppServer server = new AppServer(serverId, partitionId, systemConfigFile, partitionsConfigFile, new AppOracle(), exchangesOptimisticState);
      
      server.createInitialObjects();
      server.runStateMachine();
   }

}
