#!/usr/bin/python

import inspect
import os
import time
import sys

def script_dir():
#    returns the module path without the use of __file__.  Requires a function defined 
#    locally in the module.
#    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
   return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

if len(sys.argv) != 2 :
    print "usage: " + sys.argv[0] + " <client id>"
    sys.exit(1)

client_id = sys.argv[1] + " "

java_bin = "java -XX:+UseG1GC -cp "
app_classpath = script_dir() + "/../../../../../../../../../target/eyrie-git.jar "
client_class = "ch.usi.dslab.bezerra.eyrie.example.AppClient "
config_file = script_dir() + "/system_config.json "
partitioning_file = script_dir() + "/partitioning.json "

client_cmd = java_bin + app_classpath + client_class + client_id + config_file + partitioning_file
os.system(client_cmd)
