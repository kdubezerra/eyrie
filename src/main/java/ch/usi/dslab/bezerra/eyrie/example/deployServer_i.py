#!/usr/bin/python

import inspect
import os
import time
import sys

def script_dir():
#    returns the module path without the use of __file__.  Requires a function defined 
#    locally in the module.
#    from http://stackoverflow.com/questions/729583/getting-file-path-of-imported-module
   return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))

if len(sys.argv) != 3 :
    print "usage: " + sys.argv[0] + " <server id> <partition_id>"
    sys.exit(1)

server_id = sys.argv[1] + " "
partition = sys.argv[2] + " "


# starting servers

java_bin = "java -XX:+UseG1GC -cp "
config_file = script_dir() + "/system_config.json "
app_classpath = script_dir() + "/../../../../../../../../../target/eyrie-git.jar "
server_class = "ch.usi.dslab.bezerra.eyrie.example.AppServer "
client_class = "ch.usi.dslab.bezerra.eyrie.example.AppClient "
partitioning_file = script_dir() + "/partitioning.json "
exchangeOptBit = "1 "

debug_port = str(40000 + int(server_id))
debug_server_str = " -agentlib:jdwp=transport=dt_socket,address=127.0.0.1:" + debug_port + ",server=y,suspend=n "

silence_servers = False
#silence_servers = True

server_cmd = java_bin + app_classpath + debug_server_str + server_class + server_id + partition + config_file + partitioning_file + exchangeOptBit
if silence_servers : server_cmd += " &> /dev/null "
server_cmd += " & "
print server_cmd
os.system(server_cmd)
