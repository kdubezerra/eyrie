/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.eyrie.exec;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections4.CollectionUtils;

import ch.usi.dslab.bezerra.eyrie.CmdId;
import ch.usi.dslab.bezerra.eyrie.Command;
import ch.usi.dslab.bezerra.eyrie.LocalReplica;
import ch.usi.dslab.bezerra.eyrie.LockableMachine;
import ch.usi.dslab.bezerra.eyrie.ObjId;
import ch.usi.dslab.bezerra.eyrie.PRObject;
import ch.usi.dslab.bezerra.eyrie.Partition;
import ch.usi.dslab.bezerra.eyrie.PartitioningOracle;
import ch.usi.dslab.bezerra.eyrie.StateMachine;
import ch.usi.dslab.bezerra.netwrapper.Message;

public class CommandExecution {

   public static class Signal {
      Partition sourcePartition;
      CmdId cmdId;

      public static List<Signal> partitionListToSignalList(List<Partition> partitions, Command cmd) {
         List<Signal> slist = new ArrayList<Signal>(partitions.size());
         for (Partition p : partitions)
            slist.add(new Signal(p.getId(), cmd.getId()));
         return slist;
      }

      public Signal(int partId, CmdId cmdId) {
         this.sourcePartition = Partition.getPartition(partId);
         this.cmdId = cmdId;
      }

      @Override
      public boolean equals(Object Oother) {
         Signal other = (Signal) Oother;
         return this.sourcePartition.equals(other.sourcePartition) && this.cmdId.equals(other.cmdId);
      }
   }

   Set<ObjId> changedObjects = Collections.newSetFromMap(new ConcurrentHashMap<ObjId, Boolean>());

   Message reply = null;
   Command command;
   // boolean executed;
   // boolean mistakenOptimisticOrder;
   // boolean optimisticOrderConfirmed;
   boolean correctlyExecuted;
   List<ObjId> pendingObjectStateConfirmationSequence;

   Set<ObjId> exchangedObjects = Collections.newSetFromMap(new ConcurrentHashMap<ObjId, Boolean>());
   List<Partition> partitionsWaitingForSignals = new ArrayList<Partition>();
   List<Signal> signalsYetToReceive = new ArrayList<Signal>();
   
   List<ObjId> earlyObjectsBeforeExecution = null;

   public CommandExecution(Command cmd, LocalReplica localReplica) {
      command = cmd;
      pendingObjectStateConfirmationSequence = new ArrayList<ObjId>();
      List<Partition> otherPartitions = new ArrayList<Partition>(command.getDestinations());
      if (otherPartitions.size() > 1) {
         otherPartitions.remove(localReplica.getLocalPartition());
         signalsYetToReceive = Signal.partitionListToSignalList(otherPartitions, command);
      }
   }

   public CommandExecution(Command cmd, boolean correct) {
      command = cmd;
      correctlyExecuted = correct;
   }

   public CommandExecution(Command cmd) {
      command = cmd;
   }

   public Command getCommand() {
      return command;
   }

   // reply can only be set once
   public void setReply(Message reply) {
      if (this.reply == null)
         this.reply = reply;
   }

   public Message getReply() {
      return this.reply;
   }

   public void addExchangedObject(ObjId oid) {
      exchangedObjects.add(oid);
   }

   public void addExchangedObjects(Collection<PRObject> excObjects) {
      for (PRObject obj : excObjects)
         addExchangedObject(obj.getId());
   }

   public Set<ObjId> getExchangedObjects() {
      return exchangedObjects;
   }

   public void markChangedObject(ObjId oid) {
      changedObjects.add(oid);
   }
   
   public void tryGettingEarlyObjects() {
      PartitioningOracle oracle = StateMachine.getMachine().getOracle();
      if (oracle.knowsAllEarlyObjectIds(command))
         earlyObjectsBeforeExecution = oracle.getEarlyObjectsIds(command);
   }

   /** This method assumes that the cmdexec objects in the queue
    *  have already called tryGettingEarlyObjects(), meaning that, if
    *  obj.earlyObjectsBeforeExecution == null, it means that the objects accessed by
    *  that execution are still unknown.
    */
   public void trySendingObjectsBeforeExecution(Collection<CommandExecution> queue, LockableMachine machine) {
      tryGettingEarlyObjects();
      
      if (earlyObjectsBeforeExecution == null || LocalReplica.getLocalReplica().isPartitionMulticaster(command) == false)
         return;
      
      ArrayList<CommandExecution> pendingQueueCache;
      synchronized (queue) {
         pendingQueueCache = new ArrayList<CommandExecution>(queue);
      }

      for (CommandExecution execInQueue : pendingQueueCache) {
         if (execInQueue.earlyObjectsBeforeExecution == null)
            return;
         Collection<ObjId> conflictSet = CollectionUtils.intersection(execInQueue.earlyObjectsBeforeExecution, this.earlyObjectsBeforeExecution);
         if (conflictSet.isEmpty() == false)
            return;
      }
      
      // if arrived here, there is no possible conflict between this execution and pending executions
      
      machine.lockState();

      // multicastLocalEarlyObjects saves the sent objects to prevent sending them twice
      // - subsequent calls to multicastLocalEarlyObjects for this command execution
      //   won't send any object again
      LocalReplica.getLocalReplica().multicastLocalEarlyObjects(this, machine.isOptimistic());
      
      machine.unlockState();
   }

   public void setPendingSignals(List<Signal> pendingSignals) {
      signalsYetToReceive = pendingSignals;
   }

   public void waitPendingSignals() {
      // TODO: optimize the way the signals are handled. Maybe running through
      // the whole list everytime
      // this thread is woken up is not the best approach.

      if (signalsYetToReceive.isEmpty())
         return;

      StateMachine csm = StateMachine.getMachine();
      
      csm.getSignalsLock().lock();
      checkSignalsQueue();
      try {
         while (signalsYetToReceive.isEmpty() == false) {
            csm.getSignalsCondition().await();
            checkSignalsQueue();
         }
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
      csm.getSignalsLock().unlock();

   }

   private void checkSignalsQueue() {
      StateMachine consSM = StateMachine.getMachine();
      for (Iterator<Signal> it = signalsYetToReceive.iterator(); it.hasNext();) {
         Signal s = it.next();
         if (command.getId().equals(s.cmdId) == false) {
            System.err.println("CMDID != S.CMDID!!!!");
            System.exit(1);
         }
         if (consSM.getSignalQueue().contains(s)) {
            it.remove();
            consSM.getSignalQueue().remove(s);
         }
      }
   }

}
