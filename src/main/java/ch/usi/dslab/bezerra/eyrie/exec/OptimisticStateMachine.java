/*

 Eyrie - A library for developing SSMR-based services
 Copyright (C) 2014, University of Lugano
 
 This file is part of Eyrie.
 
 Eyrie is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
 */

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.eyrie.exec;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ch.usi.dslab.bezerra.eyrie.CmdId;
import ch.usi.dslab.bezerra.eyrie.Command;
import ch.usi.dslab.bezerra.eyrie.LocalReplica;
import ch.usi.dslab.bezerra.eyrie.LockableMachine;
import ch.usi.dslab.bezerra.eyrie.ObjId;
import ch.usi.dslab.bezerra.eyrie.PRObject;
import ch.usi.dslab.bezerra.eyrie.Partition;
import ch.usi.dslab.bezerra.eyrie.StateMachine;
import ch.usi.dslab.bezerra.netwrapper.Message;


public class OptimisticStateMachine implements Runnable, LockableMachine {
   
   public static class OSMDeliverer implements Runnable {
      boolean running;
      OptimisticStateMachine parentOSM;
      Thread delivererThread;
      
      public OSMDeliverer (OptimisticStateMachine parent) {
         parentOSM = parent;
      }
      
      public void start() {
         running = true;
         delivererThread = new Thread(this, "OSMDelivererThread");
         delivererThread.start();
      }
      
      public void stop() {
         running = false;
      }
      
      @Override
      public void run() {
         LocalReplica local = LocalReplica.getLocalReplica();
         while (running) {
            
            /*

                     // Multicast signals here already
                     StateMachine consSM = StateMachine.getMachine();
                     
                     // ==============================
                     // with super early state sending
                     CommandExecution ce = new CommandExecution(c, localReplica);
                     ce.trySendingObjectsBeforeExecution(localReplica.pendingExecutions);
                     synchronized (localReplica.pendingExecutions) {
                        localReplica.pendingExecutions.add(ce);                        
                     }
                     localReplica.conservativeExecutionQueue.put(ce);
                     // ==============================
                     // previous eyrie implementation
                     // localReplica.conservativeCommandsQueue.put(c);
                     // ==============================

             */
            
            // clause 1
            // when opt-deliver(C) ...
            
            Command nextOptDelivery = local.deliverCommandFast(true, 100);
            if (nextOptDelivery == null) continue;
            
            CommandExecution ce = new CommandExecution(nextOptDelivery, local);
            if (parentOSM.exchangesOptimisticState()) {
               ce.trySendingObjectsBeforeExecution(parentOSM.pendingOptimisticExecutions, parentOSM);
               synchronized (parentOSM.pendingOptimisticExecutions) {
                  parentOSM.pendingOptimisticExecutions.add(ce);
               }
            }
            parentOSM.optQueue.add(ce);
            
            parentOSM.wakeUpStateMachine();
         }
         
      }
      
   }
   
   public static class ExecutionStatus {
      public final static int PENDING = -1;
      public final static int OK      =  0;
      public final static int ERROR   =  1;
      private boolean knowsDestinations = false;
      private boolean localStatusSent   = false;
      Map<Integer, Integer> partitionsStatus;
      public ExecutionStatus() {
         partitionsStatus = new ConcurrentHashMap<Integer, Integer>();
      }
      public void setDestinations(List<Partition> destinations) {
         for (Partition part : destinations) {
            final int partId = part.getId();
            if (partitionsStatus.containsKey(partId) == false)
               partitionsStatus.put(partId, PENDING);
         }
         knowsDestinations = true;
      }
      void setStatus(int partitionId, int status) {
         partitionsStatus.put(partitionId, status);
      }
      int getStatus(Partition p) {
         return partitionsStatus.containsKey(p.getId()) ? partitionsStatus.get(p.getId()) : PENDING;
      }
      boolean hasAllStatus() {
         return knowsDestinations && partitionsStatus.containsValue(PENDING) == false;
      }
      boolean isFullyOK() {
         return (knowsDestinations
               && partitionsStatus.containsValue(PENDING) == false
               && partitionsStatus.containsValue(ERROR) == false);
      }
      boolean hasError() {
         return partitionsStatus.containsValue(ERROR);
      }
      void markLocalStatusAsSent() {
         localStatusSent = true;
      }
      boolean localStatusHasBeenSent() {
         return localStatusSent;
      }
   }
   
   private static OptimisticStateMachine instance;
   

   /*
    * when a mistake is detected, the optimistic state machine enters in recovery mode:
    * nothing more is executed, until all commands that were fast-executed are also conservatively
    * executed; when this happens, the optimistic state is overwritten with the conservative state
    * and the fast execution proceeds, now with a correct state
    */
   boolean   repairing;   
   StateMachine conservativeStateMachine;
   BlockingQueue<CommandExecution> optQueue;
   Queue<CommandExecution> pendingOptimisticExecutions;
   Set<CmdId> skipOpt;
   BlockingQueue<CommandExecution> consExecuted;
   BlockingQueue<CommandExecution> optExecuted;
   Set<ObjId> objectsToRepair;
   Map<CmdId, CommandExecution> doneOpt;
   Set<CmdId> doneCons;
   Lock doneQueuesLock;
   boolean optStateExchange;
   public CommandExecution currentOptimisticExecution;

   public static OptimisticStateMachine createMachine(StateMachine parentConservativeStateMachine, boolean exchangesState) {
      return new OptimisticStateMachine(parentConservativeStateMachine, exchangesState);
   }
   
   public static OptimisticStateMachine getMachine() {
      return instance;
   }
   
   private OptimisticStateMachine(StateMachine parentMachine, boolean exchangesState) {
      this.conservativeStateMachine = parentMachine;
      OptimisticStateMachine.instance = this;
      
      // java variable name // paper-algorithm variable name
      consExecuted     = new LinkedBlockingQueue<CommandExecution>(); // cons_executed
      optQueue         = new LinkedBlockingQueue<CommandExecution>(); // opt_queue
      optExecuted      = new LinkedBlockingQueue<CommandExecution>(); // opt_executed
      repairing        = false; // repairing
      skipOpt          = Collections.newSetFromMap(new ConcurrentHashMap<CmdId, Boolean>()); // skip_opt
      objectsToRepair  = Collections.newSetFromMap(new ConcurrentHashMap<ObjId, Boolean>());
      doneOpt          = new ConcurrentHashMap<CmdId, CommandExecution>();
      doneCons         = Collections.newSetFromMap(new ConcurrentHashMap<CmdId, Boolean>());
      doneQueuesLock   = new ReentrantLock();
      optStateExchange = false; // TODO: implementing baseline for now
      wakeUpSignals    = new Semaphore(0);
      executionStatusMap    = new ConcurrentHashMap<CmdId, ExecutionStatus>();
      executionStatusesMarkedForGarbageCollection = Collections.newSetFromMap(new ConcurrentHashMap<CmdId, Boolean>());
      pendingOptimisticExecutions = new LinkedList<CommandExecution>();
      stateLock = new ReentrantLock();
      setOptimisticStateExchange(exchangesState); // baseline as default
   }
   
   public void setOptimisticStateExchange(boolean doOptExchange) {
      // baseline: no opt state exchange
      // full    :    opt state exchange
      optStateExchange = doOptExchange;
      LocalReplica local = LocalReplica.getLocalReplica();
      local.setNumConsObjCollectors(doOptExchange ? 1 : 2);
   }
   
   public boolean exchangesOptimisticState() {
      return optStateExchange;
   }

   public void appendConservativeExecution(CommandExecution ce) {
      consExecuted.add(ce);
      wakeUpStateMachine();
   }

   void checkOptQueue() {

      // clause 2
      // when opt_queue \neq \emptyset and repairing == False

      if (optQueue.isEmpty() == false && repairing == false) {

         CommandExecution ce = optQueue.poll();
         if (skipOpt.contains(ce.command.id)) {
            skipOpt.remove(ce.command.id);
            markCommandExecutionAsDone(ce, false);

            // ================
            // fully optimistic
            if (exchangesOptimisticState())
               setLocalStatus(ce.command, ExecutionStatus.ERROR);
            // ================
         }
         else {
            ce.command.t_command_dequeued = System.currentTimeMillis();

            currentOptimisticExecution = ce;

            Message reply = executeOpt(ce.command);
            ce.setReply(reply);
            optExecuted.add(ce);
            LocalReplica local = LocalReplica.getLocalReplica();

            local.sendOptimisticReply(reply, ce.command);
         }

      }
   }
   
   public Message executeOpt(Command cmd) {
      cmd.rewind();
      Message optimisticReply = conservativeStateMachine.executeCommand(cmd);
//      System.out.println("Optimistic reply to command " + cmd.id + ": " + optimisticReply);
      return optimisticReply;
   }
   
   void checkOptExecutionCorrectness() {
      if (exchangesOptimisticState() == false)
         checkOptExecutionCorrectness_BASELINE();
      else
         checkOptExecutionCorrectness_FULL();
   }
   
   void checkOptExecutionCorrectness_BASELINE() {
      // clause 4
      // when opt_executed \neq \emptyset and repairing == False
      while (optExecuted.isEmpty() == false && consExecuted.isEmpty() == false && repairing == false) {
         CommandExecution  optce =  optExecuted.peek();
         CommandExecution consce = consExecuted.peek();
         if (optce.command.equals(consce.command)) {
            // optimistic execution confirmed (in baseline)
            markCommandExecutionAsDone(optce, true);
            optExecuted.remove();
            consExecuted.remove();
         }
         else {
            // mistake detected
            repairing = true;
            wakeUpStateMachine();
         }
      }
   }
   
   void checkOptExecutionCorrectness_FULL() {

      // clause 4
      // when opt_executed \neq \emptyset and repairing == False
      
      
      
      if (optExecuted.isEmpty() == false && repairing == false) {
         
         CommandExecution optce = optExecuted.peek();

         // verify execution order
         if (consExecuted.isEmpty() == false) {

            CommandExecution consce = consExecuted.peek();
            ExecutionStatus xs;
            if (optce.command.equals(consce.command)) {
               // optimistic execution order confirmed, 
               // BUT remote status confirmations are pending
               xs = setLocalStatus(optce.command, ExecutionStatus.OK);
            } else {
               // mistake detected
               xs = setLocalStatus(optce.command, ExecutionStatus.ERROR);
            }

            if (xs.hasError() || xs.isFullyOK())
               wakeUpStateMachine();
         }

         // verify remote states received
         ExecutionStatus xs = executionStatusMap.get(optce.command.id);
         if (xs != null) {

            if (xs.isFullyOK()) {
               // optce.command has been correctly opt-executed
               markCommandExecutionAsDone(optce, true);
               optExecuted.remove();
               consExecuted.remove();
            } else if (xs.hasError()) {
               repairing = true;
               for (CommandExecution ce : optExecuted)
                  setLocalStatus(ce.command, ExecutionStatus.ERROR);
            }
            wakeUpStateMachine();

         }
         
         
         
      }
      
      
      
   }
   
   void checkMatchingCommandsForRepair() {
      
      // clause 5
      // when repairing = True and there is C : C \in opt_executed \cap cons_executed
      //
      // *** fire all at once: remove the intersection of cons_executed and opt_executed from both sets
      
      if (repairing == true && optExecuted.isEmpty() == false && consExecuted.isEmpty() == false) {
         
         Set<CmdId> intersection = new HashSet<CmdId>();
         for (CommandExecution oce : optExecuted)
            for (CommandExecution cce : consExecuted)
               if (oce.command.equals(cce.command))
                  intersection.add(oce.command.id);
         
         Iterator<CommandExecution> ito = optExecuted.iterator();
         while (ito.hasNext()) {
            CommandExecution optce = ito.next();
            for (CmdId interCmdId : intersection) {
               if (optce.command.id.equals(interCmdId)) {
                  objectsToRepair.addAll(optce.changedObjects);
                  markCommandExecutionAsDone(optce, false);
                  ito.remove();
                  break;
               }
            }
         }
         
         Iterator<CommandExecution> itc = consExecuted.iterator();
         while (itc.hasNext()) {
            CommandExecution consce = itc.next();
            for (CmdId interCmdId : intersection) {
               if (consce.command.id.equals(interCmdId)) {
                  objectsToRepair.addAll(consce.changedObjects);
                  itc.remove();
                  break;
               }
            }
         }
      
      }
      
   }
   
   void checkRepairEnd() {

      // clause 6
      // when repairing = True and opt_executed = \emptyset

      if (repairing == true && optExecuted.isEmpty()) {

         conservativeStateMachine.lockState();
         for (CommandExecution ce : consExecuted) {
            objectsToRepair.addAll(ce.changedObjects);
            skipOpt.add(ce.command.id);
         }
         consExecuted.clear();
         copyConservativeState(objectsToRepair);
         conservativeStateMachine.unlockState();
         objectsToRepair.clear();
         repairing = false;
//         System.out.println("-------------\n-------------\n-------------\nOOOOOOOOOOOOOOKKKKKKKKKKKKKKKAAAAAAAAAAAAAAAAAYYYYYYYYYYYYYYY\n-------------\n-------------\n--------------");
         wakeUpStateMachine();

      }

   }

   public boolean confirmLocalOptimisticExecution(CommandExecution conservativeExecution) {

      boolean confirmed;    
      Command c = conservativeExecution.command;
      
      lockDoneQueues();
      
      CommandExecution optExec = doneOpt.get(c.id);
      if (optExec != null) {
         doneOpt.remove(c.id);
         confirmed = optExec.correctlyExecuted;
      }
      else {
         // store for garbage collection
         doneCons.add(c.id);
         confirmed = false;
      }
      
      unlockDoneQueues();      
      return confirmed;
      
   }
   
   public void startRunning() {
      running = true;
      optimisticSMThread = new Thread(this, "OptimisticStateMachine");
      optimisticSMThread.start();
      
      optDeliverer = new OSMDeliverer(this);
      optDeliverer.start();
   }
   
   public void stop() {
      optDeliverer.stop();
      running = false;
   }
   
   @Override
   public void run() {
      System.out.println("OptimisticStateMachine started.");
      while (running) {
         
         sleepUntilSomethingHappens();
         
         if (exchangesOptimisticState()) {
            this.lockState();
         }
         
         // clause 2
         // when opt_queue \neq \emptyset and repairing == False
         checkOptQueue();
         
         // clause 4
         // when opt_executed \neq \emptyset and repairing == False
         checkOptExecutionCorrectness();
         
         // clause 5
         // when repairing = True and there is C : C \in opt_executed \cap cons_executed
         checkMatchingCommandsForRepair();
         
         // clause 6
         // when repairing = True and opt_executed = \emptyset
         checkRepairEnd();

         if (exchangesOptimisticState()) {
            this.unlockState();
            garbageCollectExecutionStatuses();
         }
      }
   }

   // ================================================================================================
   // ================================================================================================
   
   // ===============
   // Private members
   // ===============

   private boolean   running;
   private Thread    optimisticSMThread;
   private Semaphore wakeUpSignals;
   private OSMDeliverer optDeliverer;
   private Map<CmdId, ExecutionStatus> executionStatusMap;
   private Set<CmdId> executionStatusesMarkedForGarbageCollection;
   private Lock stateLock;
   
   private void copyConservativeState(Set<ObjId> objects) {
//      System.out.println("Copying conservative state onto optimistic state");
      for (ObjId oid : objects) {
         PRObject obj = PRObject.getObjectById(oid);
         if (obj.isLocal()) {
//            System.out.println(":: copying conservative state of object " + obj.getId() + " onto its optimistic state");
            obj.setOptimisticState(obj.getConservativeState().deepClone());
         }
      }
   }
   
   private void markCommandExecutionAsDone(CommandExecution ce, boolean correctness) {
      
      LocalReplica local = LocalReplica.getLocalReplica();
      if (ce.command.getDestinations().size() > 1) {
         if (exchangesOptimisticState())
            local.notifyDoneWithOptDiff(ce.command.id);
         else
            local.notifyDoneWithConsDiff(ce.command.id);
      }
      
//      System.out.println(String.format("done-OPT = %d ; done-CONS = %d", doneOpt.size(), doneCons.size()));
      
      lockDoneQueues();
      
      // mark the command as executed to be used by the conservative state machine
      // to verify if the command's optimistic exection was correct;
      // in the (hopefully) rare case when C was cons-executed first, just remove
      // it from doneCons
      ce.correctlyExecuted = correctness;
      if (doneCons.contains(ce.command.id))
         doneCons.remove(ce.command.id);
      else
         doneOpt.put(ce.command.id, ce);
      
      unlockDoneQueues();

      if (exchangesOptimisticState()) {
         synchronized (pendingOptimisticExecutions) {
            pendingOptimisticExecutions.remove(ce);
         }
         markExecutionStatusForGarbageCollection(ce.command.id);
      }
      
   }
   
   private void sleepUntilSomethingHappens() {
      try {
         wakeUpSignals.acquire();
      } catch (InterruptedException e) {
         e.printStackTrace();
         System.exit(1);
      }
   }
   
   private void wakeUpStateMachine() {
      wakeUpSignals.release();
   }
   
   private void lockDoneQueues() {
      doneQueuesLock.lock();
   }
   
   private void unlockDoneQueues() {
      doneQueuesLock.unlock();
   }
   
   synchronized ExecutionStatus setLocalStatus(Command cmd, int status) {
      final LocalReplica local = LocalReplica.getLocalReplica();
      final Partition localPartition = local.getLocalPartition();
      ExecutionStatus xs = setPartitionExecutionStatus(cmd.id, localPartition.getId(), status);
      List<Partition> dests = cmd.getDestinations();
      xs.setDestinations(dests);

      if (xs.localStatusHasBeenSent() == false && dests.size() > 1 && local.isPartitionMulticaster(cmd) == true) {

         List<Partition> otherPartitions = new ArrayList<Partition>(dests);
         otherPartitions.remove(local.getLocalPartition());
   
         Message execStatus = new Message(LocalReplica.MessageType.OPTEXEC_STATUS, cmd.id, localPartition.getId(), status);
         local.multicast(otherPartitions, execStatus);
         xs.markLocalStatusAsSent();
         
      }
      
      return xs;
   }
   
   synchronized public ExecutionStatus setPartitionExecutionStatus(CmdId cmdId, int partitionId, int status) {
      ExecutionStatus xs = executionStatusMap.get(cmdId);
      if (xs == null) {
         xs = new ExecutionStatus();
         executionStatusMap.put(cmdId, xs);
      }
      xs.setStatus(partitionId, status);
      return xs;
   }
   
   synchronized public boolean checkForRemoteError(CmdId cmdId) {
      ExecutionStatus xs = executionStatusMap.get(cmdId);
      return xs != null && xs.hasError();
   }
   
   synchronized public void garbageCollectExecutionStatuses() {
      for (CmdId cmdId : executionStatusesMarkedForGarbageCollection) {
         executionStatusMap.remove(cmdId);
      }
      executionStatusesMarkedForGarbageCollection.clear();
   }
   
   synchronized public void markExecutionStatusForGarbageCollection(CmdId cmdId) {
      executionStatusesMarkedForGarbageCollection.add(cmdId);
   }
   
   public void startRepairing() {
      repairing = true;
   }

   @Override
   public void lockState() {
      stateLock.lock();
   }

   @Override
   public void unlockState() {
      stateLock.unlock();
   }

   @Override
   public boolean isOptimistic() {
      return true;
   }
   
}
